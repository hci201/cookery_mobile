
import 'package:camera/camera.dart';

abstract class ScannerService {
  static List<CameraDescription> cameras;
  static String imagePath;
  static double aspectRatio;
  static String sourceType;
  static String searchValue;
  static bool isAnalysing = false;
  static bool hideSearchResultTitle = false;

  static Future<List<CameraDescription>> getAvailableCameras() async {
    if (cameras == null) {
      // Obtain a list of the available cameras on the device.
      cameras = await availableCameras();
    }
    return cameras;
  }
}