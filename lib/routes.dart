import 'package:cookery/screens/cookbook-screen/cookbook_detail_screen.dart';
import 'package:cookery/screens/cookbook-screen/cookbook_screen.dart';
import 'package:cookery/screens/detail-screen-clone/detail_page2.dart';
import 'package:cookery/screens/home-screen/components/DetailPage.dart';
import 'package:cookery/screens/home-screen/components/searchScreen.dart';
import 'package:cookery/screens/home-screen/home_screen.dart';
import 'package:cookery/screens/main_screen.dart';
import 'package:cookery/screens/scanner-screen/review_screen.dart';
import 'package:cookery/screens/scanner-screen/scanner_screen.dart';
import 'package:cookery/screens/search-result-screen/search-result-screen.dart';
import 'package:cookery/screens/search-result-screen/suggest_result_screen.dart';
import 'package:flutter/widgets.dart';

class MainRoutes {
  static final Map<String, Widget Function(BuildContext)> routeList = {
    '/': (BuildContext context) => MainScreen(),
    '/scanner': (BuildContext context) => ScannerScreen(),
    '/review': (BuildContext context) => ReviewScreen(),
    '/recipe2': (BuildContext context) => RecipePage2(),
    '/detail': (BuildContext context) => DetailPage(),
    '/search': (BuildContext context) => SearchScreen(),
    '/search-result': (BuildContext context) => SearchResultScreen(),
    '/suggest-result': (BuildContext context) => SuggestResultScreen(),
    '/cookbook-detail': (BuildContext context) => CookbookDetailScreen(),
  };

  static final List<Widget> tabBarViewList = <Widget>[
    HomeScreen(),
    ScannerScreen(),
    CookbookScreen(),
  ];
}