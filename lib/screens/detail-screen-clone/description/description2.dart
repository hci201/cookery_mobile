import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

final String description =
    "Rau câu dẻo trái cam là món ăn dễ làm, dùng làm tráng miệng. "
    "Chỉ với những trái cam đẹp mắt, bột rau câu, đường trắng là bạn có thể thực hiện. "
    "Đặc biệt hơn, khi ăn có vị chua chua, ngọt ngọt rất thích.";

class Description2 extends StatefulWidget{
  @override
  _DescriptionState createState() => _DescriptionState();
}

class _DescriptionState extends State<Description2> {

  Widget titleSection(BuildContext context) => Stack(
    children: <Widget>[
      Container(
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(bottom: 8),
                      child: StarDisplay(value: 4),
                    ),
                    Text('Rau câu dẻo trái cam', style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 28.0,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),),
                  ],
                ),
              ),
            ],
          ),
      ),
      Positioned(
        top: -14,
        right: 27,
        child: FavoriteWidget(),
      ),
      Positioned(
        top: -14,
        right: -10,
        child: SaveWidget(),
      )
    ],
  );

  Widget timeSection = Container(
      height: 30,
      padding: EdgeInsets.only(bottom: 10),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 8),
            child: Icon(Icons.access_time, size: 15,),
          ),
          Text('35 phút',style: TextStyle(
              fontSize: 15,
              fontFamily: 'Montserrat',
          ),)
        ],
      )
  );

  Widget desDetailSection = Container(
    padding: EdgeInsets.only(bottom: 15),
    child: Wrap(
      spacing: 8.0, // gap between adjacent chips
      runSpacing: 4.0, // gap between lines
      direction: Axis.horizontal, // main axis (rows or columns)
      children: <Widget>[
        DescriptionTextWidget(text: description,),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      //height: 300,
      padding: EdgeInsets.only(left: 10, right: 10, top: 8),
      width: double.infinity,
      child: ListView(
        padding: EdgeInsets.zero,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          titleSection(context),
          timeSection,
          desDetailSection
        ],
      ),
    );
  }
}

class DescriptionTextWidget extends StatefulWidget {
  final String text;

  DescriptionTextWidget({@required this.text});

  @override
  _DescriptionTextWidgetState createState() => new _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.length > 100) {
      firstHalf = widget.text.substring(0, 100);
      secondHalf = widget.text.substring(100, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
//      height: 150,
//      padding: EdgeInsets.only(bottom: 15),
        child: GestureDetector(
          onTap: () {
            setState(() {
              flag = !flag;
            });
          },
          child: Container(
            child:secondHalf.isEmpty ? Text(firstHalf, textAlign: TextAlign.justify, style: TextStyle(fontFamily: 'Montserrat'),)
                : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf), style: TextStyle(fontFamily: 'Montserrat'), textAlign: TextAlign.justify),
                InkWell(
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: Text(
                          flag ? "Xem thêm" : "",
                          style: TextStyle(fontFamily: 'Montserrat', color: Colors.grey[300], fontSize: 15),
                        ),
                      ),
                    ],
                  ),
                ),
                !flag ? Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text('Nguyên liệu: ', style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w600)),
                        Text('(', style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300, color: Colors.green)),
                        Text('3/5', style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w600, fontSize: 15, color: Colors.green)),
                        Text(' có sẵn', style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300, color: Colors.green)),
                        Text(')', style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300, color: Colors.green)),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text('- 03 trái cam sành', style: TextStyle(fontFamily: 'Montserrat')),
                        ),
                        Icon(Icons.check, size: 24, color: Colors.green),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text('- 01 bịch rau câu Con Cá Dẻo', style: TextStyle(fontFamily: 'Montserrat')),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text('- 150g đường cát trắng', style: TextStyle(fontFamily: 'Montserrat')),
                        ),
                        Icon(Icons.check, size: 24, color: Colors.green),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text('- 200ml nước lọc', style: TextStyle(fontFamily: 'Montserrat')),
                        ),
                        Icon(Icons.check, size: 24, color: Colors.green),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text('- 01 nhánh lá húng lủi để trang trí', style: TextStyle(fontFamily: 'Montserrat')),
                        ),
                      ],
                    ),
                  ],
                ) : Container(),
              ],
            ),
          ),
        )
    );
  }
}

class StarDisplay extends StatelessWidget {
  final double value;
  const StarDisplay({Key key, this.value = 0})
      : assert(value != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(5, (index) {
        return Icon(
          index < value ? Icons.star : Icons.star_border, color: Color.fromRGBO(255, 215, 0, 5), size: 15,
        );
      }),
    );
  }
}



class SaveWidget extends StatefulWidget {
  @override
  _SaveWidgetState createState() => _SaveWidgetState();
}

class _SaveWidgetState extends State<SaveWidget> {
  bool _isSaved = false;
  String _selected;

  void _toggleSave() {
    if (_isSaved) {
      setState(() {
        _isSaved = false;
        _selected = null;
      });
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Chọn Bộ sưu tập"),
            content: DropdownButton<String>(
              value: _selected,
              items: <String>['Yêu Thích', 'Bánh Ngọt', 'Giảm Cân'].map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(value),
                    ],
                  ),
                );
              }).toList(),
              onChanged: (_) {
                _selected = _;
                _isSaved = true;
                setState(() {});
                Navigator.pop(context);
              },
            ),
          );
        }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      child: IconButton(
        icon: (_isSaved ? Icon(Icons.bookmark, color: Colors.blue,) : Icon(Icons.bookmark_border, color: Colors.black,)),
        onPressed: _toggleSave,
      ),
    );
  }
}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = false;

  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _isFavorited = false;
      } else {
        _isFavorited = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      child: IconButton(
        icon: (_isFavorited ? Icon(Icons.favorite, color: Colors.red[500],) : Icon(Icons.favorite_border, color: Colors.black,)),
        onPressed: _toggleFavorite,
      ),
    );
  }
}
