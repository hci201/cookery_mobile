import 'package:cookery/screens/back_arrow/back_arrow.dart';
import 'package:cookery/screens/detail-screen-clone/img_result/img_result2.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

var listImg = ['images/result/r5.jpg','images/result/r6.jpg','images/result/r7.jpg','images/result/r8.jpg'];

class ImageCookSliderAndZoomable2 extends StatefulWidget{

  final int indexImg ;
  ImageCookSliderAndZoomable2({
    Key key,
    @required this.indexImg}):super(key: key);

  _ImageCookSliderAndZoomableState createState() => _ImageCookSliderAndZoomableState();

}

class _ImageCookSliderAndZoomableState extends State<ImageCookSliderAndZoomable2> {

  PageController pageController;
  bool slide = false;
  int get indexImg => widget.indexImg;

  @override
  void initState() {
    super.initState();
    pageController = PageController(
    );
    //    var itemIndex = listImg.indexOf(img_1);
//    if (itemIndex != -1) pageController.jumpToPage(itemIndex);
  }

  @override
  Widget build(BuildContext context) {
    int totalCount = listImg.length;

    var data = Theme.of(context);
    return Theme(
      data: data.copyWith(
      ),
      child: Scaffold(
        body: PageView.builder(
          controller: pageController,
          itemBuilder: _buildItem,
          itemCount: totalCount,
        ),
      ),
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    var data;
    if (slide){
      data = listImg[index];
    }else{
      data = listImg[indexImg];
      slide = true;
    }
    return BigPhotoImage(
      img : data,
    );
  }
}


class BigPhotoImage extends StatefulWidget {
  final String img;
  final Widget loadingWidget;

  const BigPhotoImage({
    Key key,
    this.img,
    this.loadingWidget,
  }) : super(key: key);

  @override
  _BigPhotoImageState createState() => _BigPhotoImageState();
}

class _BigPhotoImageState extends State<BigPhotoImage> {

  String get img {
    return widget.img;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {

        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 200,
          child: Stack(
            children: <Widget>[
              PhotoView(
                imageProvider: AssetImage(img),
                minScale: PhotoViewComputedScale.contained * 1.0,
                maxScale: PhotoViewComputedScale.contained * 1.5,
                initialScale: PhotoViewComputedScale.contained * 1.0,
              ),
              CookeryBackButton()
            ],
          ),
        ),
      ),
    );
  }
}
