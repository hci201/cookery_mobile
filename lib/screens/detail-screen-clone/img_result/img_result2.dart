import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cookery/screens/detail-screen-clone/img_result/image_result_slider_zoomable2.dart';


var listImg = ['images/result/r5.jpg','images/result/r6.jpg','images/result/r7.jpg','images/result/r8.jpg'];

class ImageResult2 extends StatefulWidget{
  _ImageResultState createState() => _ImageResultState();
}

class _ImageResultState extends State<ImageResult2>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Container(
        height: 280,
        width: double.infinity,
        child: PhotoPreviewPage(),
      color: Colors.white,
      );
  }
}

class PhotoPreviewPage extends StatefulWidget {
  @override
  _PhotoPreviewPageState createState() => _PhotoPreviewPageState();
}

class _PhotoPreviewPageState extends State<PhotoPreviewPage> {
  int selectedIndex;
  PageController pageController;

  @override
  void initState() {
    super.initState();
    selectedIndex = 0;
    pageController = PageController(
    );
  }

  @override
  Widget build(BuildContext context) {
    int totalCount = listImg.length;

    var data = Theme.of(context);
    return Theme(
      data: data.copyWith(
      ),
      child: Scaffold(
        body: Container(
          child: PageView.builder(
            controller: pageController,
            itemBuilder: _buildItem,
            itemCount: totalCount,
          ),
        ),
        bottomSheet: _buildThumb(),
      ),
    );
  }

  // Show big img
  Widget _buildItem(BuildContext context, int index) {
    var data = listImg[index];
    return BigPhotoImage(
      img : data,
    );
  }

  // Show list small img at bottom
  Widget _buildThumb() {
    return StreamBuilder(
      builder: (ctx, snapshot) => Container(
        height: 100.0,
        padding: EdgeInsets.all(8.0),
        child: Container(
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            itemBuilder: _buildThumbItem,
            itemCount: listImg.length,
            scrollDirection: Axis.horizontal,
          ),
        ),
      ),
    );
  }

  Widget _buildThumbItem(BuildContext context, int index) {
    var item = listImg[index];
    return Container(
      child: GestureDetector(
        onTap: () => changeSelected(item, index),
        child: Container(
          width: 130.0,
          height: 50.0,
          padding: EdgeInsets.all(4.0),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(width: selectedIndex == index ? 1.5 : 0.5, color: Colors.red),
              borderRadius: BorderRadius.circular(5.0),
              image:DecorationImage(
                image:AssetImage(listImg[index]),
                  fit: BoxFit.cover
              )
            ),
            ),
          ),
      ),
    );
  }

  // Choose small img --> change big img
  void changeSelected(String img, int index) {
    var itemIndex = listImg.indexOf(img);
    if (itemIndex != -1) pageController.jumpToPage(itemIndex);
    setState(() {
      selectedIndex = itemIndex;
    });
  }

}

class BigPhotoImage extends StatefulWidget {
  final String img;
  final Widget loadingWidget;

  const BigPhotoImage({
    Key key,
    this.img,
    this.loadingWidget,
  }) : super(key: key);

  @override
  _BigPhotoImageState createState() => _BigPhotoImageState();
}

class _BigPhotoImageState extends State<BigPhotoImage>{

  String get img{
    return widget.img;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      child: GestureDetector(
        onTap: (){
          var index = listImg.indexOf(img);
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) =>
                    ImageCookSliderAndZoomable2(indexImg:index),
              ));
        },
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(img),
                fit: BoxFit.cover
              )
            ),
          )
      ),
    );
  }

}
