import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player/youtube_player.dart';

class TabVideo2 extends StatefulWidget {
  TabVideo2({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TabVideo2State createState() => _TabVideo2State();
}

class _TabVideo2State extends State<TabVideo2> {
  static const platform = const MethodChannel("np.com.sarbagyastha.example");
  VideoPlayerController _videoController;
  // String position = "Get Current Position";
  //String status = "Get Player Status";
  // String videoDuration = "Get Video Duration";
  String _source = "iIQwVd8Z5ek";
  // bool isMute = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: SingleChildScrollView(
        child: Column(
         mainAxisAlignment: MainAxisAlignment.center,
         mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            SizedBox(
              height: 28,
            ),
            YoutubePlayer(
              context: context,
              source: _source,
              quality: YoutubeQuality.HD,
              aspectRatio: 5/3.75,
              autoPlay: false,
              loop: false,
              reactToOrientationChange: true,
              startFullScreen: false,
              controlsActiveBackgroundOverlay: true,
              controlsTimeOut: Duration(seconds: 4),
              playerMode: YoutubePlayerMode.DEFAULT,
              callbackController: (controller) {
                _videoController = controller;
              },
              onError: (error) {
                print(error);
              },
              //onVideoEnded: () => _showThankYouDialog(),
            ),
          ],
        ),
      ),
    );
  }
}
