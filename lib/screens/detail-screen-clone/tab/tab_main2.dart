import 'package:flutter/material.dart';
import 'tab_recipe2.dart';
import 'tab_video2.dart';
import 'tab_note2.dart';

class TabMain2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //build function returns a "Widget"
    final tabController = DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(30),
          child: Material(
            color: Colors.red,
            child: TabBar(
              //: Colors.red,
//            elevation: 0,
//            bottom: TabBar(
                unselectedLabelColor: Colors.white,
                indicatorSize: TabBarIndicatorSize.label,
                labelColor: Colors.red,
                indicator: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                    color: Colors.white
                ),
//          indicator: BoxDecoration(
//            gradient: LinearGradient(colors: [Colors.red, Colors.red]),
//            borderRadius: BorderRadius.circular(50),
//            color: Colors.red
//          ),
                tabs: [
                  Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Công thức'),
                      )
                  ),
                  Tab(
                    child: Align(
                        alignment: Alignment.center,
                        child: Text('Video')
                    ),
                  ),
                  Tab(
                    child: Align(
                        alignment: Alignment.center,
                        child: Text('Ghi chú')
                    ),
                  ),
                ]),
          ),
//            )
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            TabRecipe2(),
            TabVideo2(),
            TabNote2(),
          ]
        ),
      ),
    );
    return Container(
        height: 400,
        child: tabController,
    );
  }
}