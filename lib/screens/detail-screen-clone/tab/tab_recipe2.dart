import 'package:flutter/material.dart';

final String description =
    "Bước 1: \n"
    "Rửa sạch thịt heo, cắt nhỏ vừa ăn. Bóc vỏ hành tím, chẻ nhỏ múi cau. Cắt ớt thành những khoanh nhỏ. Ướp thịt heo với 2 muỗng canh nước mắm, 1/2 muỗng cà phê muối, 2 muỗng cà phê đường, 1 muỗng cà phê bột ngọt, 3 muỗng cà phê tỏi, ớt cắt nhỏ và 1.5 muỗng cà phê tiêu trong khoảng 15-20 phút.\n\n"
    "Bước 2: \n"
    "Hòa tan 1 muỗng cà phê nước màu với 1 chén nhỏ nước ấm rồi cho vào tô thịt đã ướp, trộn đều lên sẽ làm thịt kho tiêu trông đẹp mắt và ngon miệng hơn\n\n"
    "Bước 3: \n"
    "Cách làm thịt kho tiêu: Phi thơm 1 muỗng cà phê tỏi băm với 3 muỗng canh dầu ăn rồi gắp thịt đã ướp cho vào nồi đảo đều tay. \n\n"
    "Bước 4: \n"
    "Khi những miếng thịt kho săn lại thì thêm nước ướp thịt vào, mở lửa to. Đun đến khi nước gần cạn, sệt lại và bám lên thịt kho tiêu là hoàn tất. \n\n"
    "Bước 5: \n"
    "Cho thịt kho tiêu ra đĩa và thưởng thức với cơm trắng rất ngon. Cách làm thịt kho tiêu với cơm nóng hay cơm nguội đều ngon hết sẩy. Để bữa cơm đầy đủ chất dinh dưỡng hơn thì bạn có thể chuẩn bị thêm dưa leo, rau sống, chấm cùng nước thịt kho rất tiện lợi và ngon miệng.";
class TabRecipe2 extends StatefulWidget{
  @override
  _TabRecipe2State createState() => _TabRecipe2State();
}

class _TabRecipe2State extends State<TabRecipe2> {

  Widget descriptionSection = Container(
    height: 50,
    padding: EdgeInsets.only(top: 20),
    child: Text('Công thức chi tiết', style: new TextStyle(
      fontSize: 18.0,
      color: Colors.black,
      fontWeight: FontWeight.bold,
    ),),
  );

  Widget desDetailSection = Container(
    height: 550,
//    child: Wrap(
//      spacing: 8.0, // gap between adjacent chips
//      runSpacing: 4.0, // gap between lines
//      direction: Axis.horizontal, // main axis (rows or columns)
//      children: <Widget>[
//        DescriptionTextWidget(text: description,),
//      ],
//    ),
    child: DescriptionTextWidget(text: description),
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(left: 10, right: 10),
      width: double.infinity,
      child: desDetailSection,
//      child:ListView(
//        physics: NeverScrollableScrollPhysics(),
//        shrinkWrap: true,
//        children: <Widget>[
//          Container(
//            child: descriptionSection,
//          ),
//          Container(
//              child:  desDetailSection
//          ),
//        ],
//      ),
    );
  }
}


class DescriptionTextWidget extends StatefulWidget {
  final String text;

  DescriptionTextWidget({@required this.text});

  @override
  _DescriptionTextWidgetState createState() => new _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {

  var _index = 0;

  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.length > 50) {
      firstHalf = widget.text.substring(0, 350);
      secondHalf = widget.text.substring(350, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
//    return new Container(
////      height: 150,
////      padding: EdgeInsets.only(bottom: 15),
//        child: GestureDetector(
//          onTap: () {
//            setState(() {
//              flag = !flag;
//            });
//          },
//          child: Container(
//            child:secondHalf.isEmpty ? new Text(firstHalf)
//                : new Column(
//              children: <Widget>[
//                new Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf)),
//                new InkWell(
//                  child: new Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      new Text(
//                        flag ? "xem thêm" : "",
//                        style: new TextStyle(color: Colors.grey, fontSize: 15),
//                      ),
//                    ],
//                  ),
//                ),
//              ],
//            ),
//          ),
//        )
//    );
    return Container(
      //constraints: BoxConstraints.expand(height: 500),
      //height: 500,
      child: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Container(
            child: Padding(
              padding: const EdgeInsets.only(top: 25),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          shape: BoxShape.circle,
                        ),
                        child: Center(child: Text('1', style: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 12))),
                      ),
                      Text('  -  '),
                      Text('Bước 1:', style: TextStyle(fontFamily: 'Montserrat', fontSize: 13, fontWeight: FontWeight.w600)),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 9, top: 5, bottom: 5),
                    child: Container(
                      padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 30),
                      decoration: BoxDecoration(
                        border: Border(
                          left: BorderSide(width: 1.5, color: Theme.of(context).primaryColor),
                        ),
                      ),
                      child: Text(
                        'Trái cam rửa sạch. 1 trái dùng để trang trí. 2 trái còn lại dùng dao cắt miếng hình tròn trên phía đầu.',
                        textAlign: TextAlign.justify,
                        style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(child: Text('2', style: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 12))),
                    ),
                    Text('  -  '),
                    Text('Bước 2:', style: TextStyle(fontFamily: 'Montserrat', fontSize: 13, fontWeight: FontWeight.w600)),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 9, top: 5, bottom: 5),
                  child: Container(
                    padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 30),
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(width: 1.5, color: Theme.of(context).primaryColor),
                      ),
                    ),
                    child: Text(
                      'Dùng dao mũi nhọn khoét lấy ruột cam, cho ra tô.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(child: Text('3', style: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 12))),
                    ),
                    Text('  -  '),
                    Text('Bước 3:', style: TextStyle(fontFamily: 'Montserrat', fontSize: 13, fontWeight: FontWeight.w600)),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 9, top: 5, bottom: 5),
                  child: Container(
                    padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 30),
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(width: 1.5, color: Theme.of(context).primaryColor),
                      ),
                    ),
                    child: Text(
                      'Cho ruột cam vào rây. Dùng muỗng dầm đều cho ra hết nước, lấy nước cam, bỏ bã.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(child: Text('4', style: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 12))),
                    ),
                    Text('  -  '),
                    Text('Bước 4:', style: TextStyle(fontFamily: 'Montserrat', fontSize: 13, fontWeight: FontWeight.w600)),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 9, top: 5, bottom: 5),
                  child: Container(
                    padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 30),
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(width: 1.5, color: Theme.of(context).primaryColor),
                      ),
                    ),
                    child: Text(
                      'Trộn đều 4 muỗng cà phê đường trắng, 1 muỗng cà phê bột rau câu trong chén.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(child: Text('5', style: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 12))),
                    ),
                    Text('  -  '),
                    Text('Bước 5:', style: TextStyle(fontFamily: 'Montserrat', fontSize: 13, fontWeight: FontWeight.w600)),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 9, top: 5, bottom: 5),
                  child: Container(
                    padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 30),
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(width: 1.5, color: Theme.of(context).primaryColor),
                      ),
                    ),
                    child: Text(
                      'Cho nước cam vào nồi. Thêm 200ml nước cốt cam vào. Bắc lên bếp.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(child: Text('6', style: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 12))),
                    ),
                    Text('  -  '),
                    Text('Bước 6:', style: TextStyle(fontFamily: 'Montserrat', fontSize: 13, fontWeight: FontWeight.w600)),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 9, top: 5, bottom: 5),
                  child: Container(
                    padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 30),
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(width: 1.5, color: Theme.of(context).primaryColor),
                      ),
                    ),
                    child: Text(
                      'Cho hỗn hợp bột rau câu vào, khuấy đều đến khi hỗn hợp sôi, có độ sệt lại.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(child: Text('7', style: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 12))),
                    ),
                    Text('  -  '),
                    Text('Bước 7:', style: TextStyle(fontFamily: 'Montserrat', fontSize: 13, fontWeight: FontWeight.w600)),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 9, top: 5, bottom: 5),
                  child: Container(
                    padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 30),
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(width: 1.5, color: Theme.of(context).primaryColor),
                      ),
                    ),
                    child: Text(
                      'Múc hỗn hợp rau câu vào bên trong trái cam, để nguội. Đặt trong 1 cái chén để cố định.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(child: Text('8', style: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 12))),
                    ),
                    Text('  -  '),
                    Text('Bước 8:', style: TextStyle(fontFamily: 'Montserrat', fontSize: 13, fontWeight: FontWeight.w600)),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 9, top: 5, bottom: 5),
                  child: Container(
                    padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 30),
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(width: 1.5, color: Theme.of(context).primaryColor),
                      ),
                    ),
                    child: Text(
                      'Trang trí lá húng lũi lên rau câu cho đẹp mắt. 1 trái cam còn lại cắt lát mỏng hình tròn, trang trí xung quanh là xong.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
//       Stepper(
//         physics: NeverScrollableScrollPhysics(),
//         steps: [
//           Step(
//               title: Text('Bước 1'),
//               content: Text('Trái cam rửa sạch. 1 trái dùng để trang trí. 2 trái còn lại dùng dao cắt miếng hình tròn trên phía đầu.'),
//               //isActive: true
//           ),
//           Step(
//               title: Text('Bước 2'),
//               content: Text('Dùng dao mũi nhọn khoét lấy ruột cam, cho ra tô.'),
//             //  isActive: true
//           ),
//           Step(
//               title: Text('Bước 3'),
//               content: Text('Cho ruột cam vào rây. Dùng muỗng dầm đều cho ra hết nước, lấy nước cam, bỏ bã.'),
//              // isActive: true
//           ),
//           Step(
//               title: Text('Bước 4'),
//               content: Text('Trộn đều 4 muỗng cà phê đường trắng, 1 muỗng cà phê bột rau câu trong chén.'),
//            //   isActive: true
//           ),
//           Step(
//               title: Text('Bước 5'),
//               content: Text('Cho nước cam vào nồi. Thêm 200ml nước cốt cam vào. Bắc lên bếp.'),
//          //     isActive: true
//           ),
//           Step(
//               title: Text('Bước 6'),
//               content: Text('Cho hỗn hợp bột rau câu vào, khuấy đều đến khi hỗn hợp sôi, có độ sệt lại.'),
//            //   isActive: true
//           ),
//           Step(
//               title: Text('Bước 7'),
//               content: Text('Múc hỗn hợp rau câu vào bên trong trái cam, để nguội. Đặt trong 1 cái chén để cố định.'),
//           //    isActive: true
//           ),
//           Step(
//               title: Text('Bước 8'),
//               content: Text('Trang trí lá húng lũi lên rau câu cho đẹp mắt. 1 trái cam còn lại cắt lát mỏng hình tròn, trang trí xung quanh là xong.'),
//             //  isActive: true
//           )
//         ],
// //        currentStep: _index,
// //        onStepTapped: (index) {
// //          setState(() {
// //            _index = index;
// //          });
// //        },
//         controlsBuilder: (BuildContext context,
//             {VoidCallback onStepContinue, VoidCallback onStepCancel}) =>
//             Container(),
//       ),
    );
  }
}