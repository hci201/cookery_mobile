import 'package:cookery/screens/back_arrow/back_arrow.dart';
import 'package:cookery/screens/detail-screen-clone/description/description2.dart';
import 'package:flutter/material.dart';
import 'package:cookery/screens/detail-screen-clone/img_result/img_result2.dart';
import 'package:cookery/screens/detail-screen-clone/img_material/img_material2.dart';
import 'package:cookery/screens/detail-screen-clone/tab/tab_main2.dart';

class RecipePage2 extends StatefulWidget{
  @override
  _RecipePageState createState() => _RecipePageState();
}

class _RecipePageState extends State<RecipePage2>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            // color: Color.fromRGBO(211, 211, 211, 1.0),
            child: CustomScrollView(
              physics: BouncingScrollPhysics(),
                slivers: <Widget>[
                  SliverList(
                    delegate: SliverChildListDelegate(
                      [
                        Stack(
                          children: <Widget>[
                            ImageResult2(),
                            CookeryBackButton(),
                          ],
                        ),
                        Container(
                          height: 0.5,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[300],
                                blurRadius: 3,
                                offset: Offset(-0.5, -0.8),
                              ),
                              BoxShadow(
                                color: Colors.grey[300],
                                blurRadius: 3,
                                offset: Offset(0.5, 0.8),
                              ),
                            ],
                          ),
                          child: Description2(),
                        ),
                        Container(
                          height: 6,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[300],
                                blurRadius: 3,
                                offset: Offset(-0.5, -0.8),
                              ),
                            ],
                          ),
                          child: TabMain2(),
                        ),
                        Container(
                          height: 0.5,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[300],
                                blurRadius: 3,
                                offset: Offset(-0.5, -0.8),
                              ),
                            ],
                          ),
                          child: ImgMaterial2(),
                        ),
                      ],
                    ),
                  ),

//                SliverList(
//                  delegate: SliverChildListDelegate(
//                    [
//                      Stack(
//                        children: <Widget>[
//                          Container(
//                            color: Colors.white,
//                            padding: const EdgeInsets.all(5.0),
//                            child: Text('Hình ảnh nguyên liệu', style: new TextStyle(
//                              fontSize: 15.0,
//                              color: Colors.black,
//                              fontWeight: FontWeight.bold,
//                            ),
//                            ),
//                          ),
//                          ImgMaterial()
//                        ],
//                      )
//                    ],
//                  ),
//                ),
//
//                SliverList(
//                  delegate: SliverChildListDelegate(
//                    [
//                      Stack(
//                        children: <Widget>[
//                          Description()
//                        ],
//                      )
//                    ],
//                  ),
//                ),
                ]
            )
        )
    );

  }

//@override
//  Widget build(BuildContext context) {
//
//    return Scaffold(
//      body:  ListView(
//          children: <Widget>[
//            Row(
//              children: <Widget>[
//                ImageResult(),
//                Description(),
//                //ImgMaterial(),
//              ],
//            )
//          ],
//        ),
//    );
//  }


//    @override
//  Widget build(BuildContext context) {
//
//      return Container(
//        child: Stack(
//          children: <Widget>[
//            Container(
//                    padding: const EdgeInsets.all(5.0),
//                    child: Text('Hình ảnh nguyên liệu', style: new TextStyle(
//                      fontSize: 12.0,
//                      color: Colors.black,
//                      fontWeight: FontWeight.bold,
//                    ),
//                    ),
//                  ),
//            ImageResult(),
//            ImgMaterial()
//          ],
//        ),
//      );
//  }
}