import 'package:cookery/routes.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedPageIndex;

  @override
  void initState() {
    _selectedPageIndex = 0;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MainRoutes.tabBarViewList[_selectedPageIndex],
      bottomNavigationBar: Container(
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(40), topRight: Radius.circular(40)),
          boxShadow: [BoxShadow(
            blurRadius: 3,
            color: Colors.grey.withOpacity(0.5),
          )],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(40), topRight: Radius.circular(40)),
          child: BottomNavigationBar(
            currentIndex: _selectedPageIndex,
            type: BottomNavigationBarType.fixed,
            iconSize: 30,
            elevation: 10,
            /*showUnselectedLabels: true,*/
            showUnselectedLabels: false,
            selectedItemColor: Colors.red,
            /*selectedFontSize: 10,
            unselectedItemColor: Colors.grey.withOpacity(0.8),
            unselectedFontSize: 10,*/
            onTap: (index) {
              if (index != 1) {
                setState(() {
                  _selectedPageIndex = index;
                });
              } else {
                Navigator.pushNamed(context, '/scanner');
              }
            },
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.wb_incandescent),
                title: Text('Gợi Ý', style: TextStyle(fontSize: 10)),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.filter_center_focus),
                title: Text('Máy Quét', style: TextStyle(fontSize: 10)),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.bookmark),
                title: Text('Lưu Trữ', style: TextStyle(fontSize: 10)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}