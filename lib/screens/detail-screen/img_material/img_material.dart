import 'package:flutter/material.dart';
import 'package:cookery/screens/detail-screen/img_material/img_material_slider_zoomable.dart';
var listImg = ['images/material/m1.jpg','images/material/m2.jpg','images/material/m3.jpg','images/material/m4.jpg','images/material/m5.jpg','images/material/m6.jpg'];

//Now use stateful Widget = Widget has properties which can be changed
class ImgMaterial extends StatefulWidget {

  @override
  _ImgMaterialState createState() => _ImgMaterialState();
}

class _ImgMaterialState extends State<ImgMaterial> {

  Widget _titleSection = Container(
//    height: 50,
    padding: EdgeInsets.only(bottom: 15),
    child: Text('Hình ảnh nguyên liệu', style: new TextStyle(
      fontSize: 20.0,
      color: Colors.black,
      fontWeight: FontWeight.bold,
    ),),
  );

  Widget _imageMaterial = Container(
    child: GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: listImg.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 1,
          crossAxisSpacing: 1,
        ),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) =>
                          ImageMaterialSliderAndZoomable(indexImg:index),
                    )
                );
            },
            child: Image.asset(listImg[index], fit: BoxFit.cover,),
          );
        }
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(10),
      width: double.infinity,
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          _titleSection,
          _imageMaterial
        ],
      ),
    );
 }
}
