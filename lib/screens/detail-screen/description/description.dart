import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

final String description =
    "Thịt Kho Tiêu là món ăn ngon phổ biến trong mâm cơm của người Việt. "
    "Cách nấu thịt kho tiêu cay thơm, ngấm vị mặn ngọt đậm đà thì bạn cần thái nhỏ thịt heo, "
    "ướp đầy đủ các gia vị trong thời gian đủ dài."
    "Với cách làm thịt kho tiêu này, bữa cơm gia đình sẽ thêm hấp dẫn."
    "Hãy lưu ngay cách kho thịt nạc hấp dẫn này cho bữa cơm gia đình nhé!";

class Description extends StatefulWidget{
  @override
  _DescriptionState createState() => _DescriptionState();
}

class _DescriptionState extends State<Description> {

  Widget titleSection(BuildContext context) => Stack(
    children: <Widget>[
      Container(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 8),
                    child: StarDisplay(value: 4),
                  ),
                  Text('Rau câu dẻo trái cam', style: new TextStyle(
                    fontSize: 28.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),),
                ],
              ),
            ),
          ],
        ),
      ),
      Positioned(
        top: -14,
        right: 27,
        child: FavoriteWidget(),
      ),
      Positioned(
        top: -14,
        right: -10,
        child: SaveWidget(),
      )
    ],
  );

  Widget timeSection = Container(
      height: 30,
      padding: EdgeInsets.only(bottom: 10),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 8),
            child: Icon(Icons.access_time, size: 15,),
          ),
          Text('35 phút',style: TextStyle(
              fontSize: 15
          ),)
        ],
      )
  );

  Widget desDetailSection = Container(
    padding: EdgeInsets.only(bottom: 15),
    child: Wrap(
      spacing: 8.0, // gap between adjacent chips
      runSpacing: 4.0, // gap between lines
      direction: Axis.horizontal, // main axis (rows or columns)
      children: <Widget>[
        DescriptionTextWidget(text: description,),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      //height: 300,
      padding: EdgeInsets.only(left: 10, right: 10, top: 8),
      width: double.infinity,
      child: ListView(
        padding: EdgeInsets.zero,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          titleSection(context),
          timeSection,
          desDetailSection
        ],
      ),
    );
  }
}

class DescriptionTextWidget extends StatefulWidget {
  final String text;

  DescriptionTextWidget({@required this.text});

  @override
  _DescriptionTextWidgetState createState() => new _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.length > 50) {
      firstHalf = widget.text.substring(0, 81);
      secondHalf = widget.text.substring(81, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
//      height: 150,
//      padding: EdgeInsets.only(bottom: 15),
        child: GestureDetector(
          onTap: () {
            setState(() {
              flag = !flag;
            });
          },
          child: Container(
            child:secondHalf.isEmpty ? new Text(firstHalf)
                : new Column(
              children: <Widget>[
                new Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf)),
                new InkWell(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        flag ? "xem thêm" : "",
                        style: new TextStyle(color: Colors.grey, fontSize: 15),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )
    );
  }
}

class StarDisplay extends StatelessWidget {
  final double value;
  const StarDisplay({Key key, this.value = 0})
      : assert(value != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(5, (index) {
        return Icon(
          index < value ? Icons.star : Icons.star_border, color: Color.fromRGBO(255, 215, 0, 5), size: 15,
        );
      }),
    );
  }
}



class SaveWidget extends StatefulWidget {
  @override
  _SaveWidgetState createState() => _SaveWidgetState();
}

class _SaveWidgetState extends State<SaveWidget> {
  bool _isSaved = false;

  void _toggleSave() {
    setState(() {
      if (_isSaved) {
        _isSaved = false;
      } else {
        _isSaved = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      child: IconButton(
        icon: (_isSaved ? Icon(Icons.bookmark, color: Colors.blue,) : Icon(Icons.bookmark_border, color: Colors.black,)),
        onPressed: _toggleSave,
      ),
    );
  }
}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = false;

  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _isFavorited = false;
      } else {
        _isFavorited = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      child: IconButton(
        icon: (_isFavorited ? Icon(Icons.favorite, color: Colors.red[500],) : Icon(Icons.favorite_border, color: Colors.black,)),
        onPressed: _toggleFavorite,
      ),
    );
  }
}