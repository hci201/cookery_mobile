import 'package:flutter/material.dart';

class TabNote extends StatefulWidget{
  _TabNoteState createState() => _TabNoteState();
}

class _TabNoteState extends State<TabNote>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(7),
      child: TextField(
        maxLines: 50,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: 'Ghi chú...'
        ),
      ),
    );
  }
}