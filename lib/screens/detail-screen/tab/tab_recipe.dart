import 'package:flutter/material.dart';

final String description =
    "Bước 1: \n"
    "Rửa sạch thịt heo, cắt nhỏ vừa ăn. Bóc vỏ hành tím, chẻ nhỏ múi cau. Cắt ớt thành những khoanh nhỏ. Ướp thịt heo với 2 muỗng canh nước mắm, 1/2 muỗng cà phê muối, 2 muỗng cà phê đường, 1 muỗng cà phê bột ngọt, 3 muỗng cà phê tỏi, ớt cắt nhỏ và 1.5 muỗng cà phê tiêu trong khoảng 15-20 phút.\n\n"
    "Bước 2: \n"
    "Hòa tan 1 muỗng cà phê nước màu với 1 chén nhỏ nước ấm rồi cho vào tô thịt đã ướp, trộn đều lên sẽ làm thịt kho tiêu trông đẹp mắt và ngon miệng hơn\n\n"
    "Bước 3: \n"
    "Cách làm thịt kho tiêu: Phi thơm 1 muỗng cà phê tỏi băm với 3 muỗng canh dầu ăn rồi gắp thịt đã ướp cho vào nồi đảo đều tay. \n\n"
    "Bước 4: \n"
    "Khi những miếng thịt kho săn lại thì thêm nước ướp thịt vào, mở lửa to. Đun đến khi nước gần cạn, sệt lại và bám lên thịt kho tiêu là hoàn tất. \n\n"
    "Bước 5: \n"
    "Cho thịt kho tiêu ra đĩa và thưởng thức với cơm trắng rất ngon. Cách làm thịt kho tiêu với cơm nóng hay cơm nguội đều ngon hết sẩy. Để bữa cơm đầy đủ chất dinh dưỡng hơn thì bạn có thể chuẩn bị thêm dưa leo, rau sống, chấm cùng nước thịt kho rất tiện lợi và ngon miệng.";
class TabRecipe extends StatefulWidget{
  @override
  _TabRecipeState createState() => _TabRecipeState();
}

class _TabRecipeState extends State<TabRecipe> {

  Widget descriptionSection = Container(
    height: 50,
    padding: EdgeInsets.only(top: 20),
    child: Text('Công thức chi tiết', style: new TextStyle(
      fontSize: 18.0,
      color: Colors.black,
      fontWeight: FontWeight.bold,
    ),),
  );

  Widget desDetailSection = Container(
    height: 500,
//    child: Wrap(
//      spacing: 8.0, // gap between adjacent chips
//      runSpacing: 4.0, // gap between lines
//      direction: Axis.horizontal, // main axis (rows or columns)
//      children: <Widget>[
//        DescriptionTextWidget(text: description,),
//      ],
//    ),
    child: DescriptionTextWidget(text: description),
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(left: 10, right: 10),
      width: double.infinity,
      child:ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: <Widget>[
          Container(
            child: descriptionSection,
          ),
          Container(
              child:  desDetailSection
          ),
        ],
      ),
    );
  }
}


class DescriptionTextWidget extends StatefulWidget {
  final String text;

  DescriptionTextWidget({@required this.text});

  @override
  _DescriptionTextWidgetState createState() => new _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {

  var _index = 0;

  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.length > 50) {
      firstHalf = widget.text.substring(0, 350);
      secondHalf = widget.text.substring(350, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
//    return new Container(
////      height: 150,
////      padding: EdgeInsets.only(bottom: 15),
//        child: GestureDetector(
//          onTap: () {
//            setState(() {
//              flag = !flag;
//            });
//          },
//          child: Container(
//            child:secondHalf.isEmpty ? new Text(firstHalf)
//                : new Column(
//              children: <Widget>[
//                new Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf)),
//                new InkWell(
//                  child: new Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      new Text(
//                        flag ? "xem thêm" : "",
//                        style: new TextStyle(color: Colors.grey, fontSize: 15),
//                      ),
//                    ],
//                  ),
//                ),
//              ],
//            ),
//          ),
//        )
//    );
  return Container(
    //constraints: BoxConstraints.expand(height: 500),
    //height: 500,
    child: Stepper(
      physics: ClampingScrollPhysics(),
      steps: [
        Step(
          title: Text('Bước 1'),
          content: Text('Rửa sạch thịt heo, cắt nhỏ vừa ăn. Bóc vỏ hành tím, chẻ nhỏ múi cau. Cắt ớt thành những khoanh nhỏ. Ướp thịt heo với 2 muỗng canh nước mắm, 1/2 muỗng cà phê muối, 2 muỗng cà phê đường, 1 muỗng cà phê bột ngọt, 3 muỗng cà phê tỏi, ớt cắt nhỏ và 1.5 muỗng cà phê tiêu trong khoảng 15-20 phút.')
        ),
        Step(
            title: Text('Bước 2'),
            content: Text('Rửa sạch thịt heo, cắt nhỏ vừa ăn. Bóc vỏ hành tím, chẻ nhỏ múi cau. Cắt ớt thành những khoanh nhỏ. Ướp thịt heo với 2 muỗng canh nước mắm, 1/2 muỗng cà phê muối, 2 muỗng cà phê đường, 1 muỗng cà phê bột ngọt, 3 muỗng cà phê tỏi, ớt cắt nhỏ và 1.5 muỗng cà phê tiêu trong khoảng 15-20 phút.')
        ),
        Step(
            title: Text('Bước 3'),
            content: Text('Cách làm thịt kho tiêu: Phi thơm 1 muỗng cà phê tỏi băm với 3 muỗng canh dầu ăn rồi gắp thịt đã ướp cho vào nồi đảo đều tay.')
        ),
        Step(
            title: Text('Bước 4'),
            content: Text('Cho thịt kho tiêu ra đĩa và thưởng thức với cơm trắng rất ngon. Cách làm thịt kho tiêu với cơm nóng hay cơm nguội đều ngon hết sẩy. Để bữa cơm đầy đủ chất dinh dưỡng hơn thì bạn có thể chuẩn bị thêm dưa leo, rau sống, chấm cùng nước thịt kho rất tiện lợi và ngon miệng.')
        )
      ],
      currentStep: _index,
      onStepTapped: (index) {
        setState(() {
          _index = index;
        });
      },
      controlsBuilder: (BuildContext context,
          {VoidCallback onStepContinue, VoidCallback onStepCancel}) =>
          Container(),
    ),
    );
  }
}