import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player/youtube_player.dart';

class TabVideo extends StatefulWidget {
  TabVideo({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TabVideoState createState() => _TabVideoState();
}

class _TabVideoState extends State<TabVideo> {
  static const platform = const MethodChannel("np.com.sarbagyastha.example");
  VideoPlayerController _videoController;
  // String position = "Get Current Position";
  //String status = "Get Player Status";
  // String videoDuration = "Get Video Duration";
  String _source = "0XUasOt5kvI";
  // bool isMute = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
         // mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            YoutubePlayer(
              context: context,
              source: _source,
              quality: YoutubeQuality.HD,
              aspectRatio: 5/3.75,
              autoPlay: false,
              loop: false,
              reactToOrientationChange: true,
              startFullScreen: false,
              controlsActiveBackgroundOverlay: true,
              controlsTimeOut: Duration(seconds: 4),
              playerMode: YoutubePlayerMode.DEFAULT,
              callbackController: (controller) {
                _videoController = controller;
              },
              onError: (error) {
                print(error);
              },
              //onVideoEnded: () => _showThankYouDialog(),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
