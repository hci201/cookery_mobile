import 'package:cookery/screens/back_arrow/back_arrow.dart';
import 'package:cookery/screens/detail-screen/description/description.dart';
import 'package:flutter/material.dart';
import 'package:cookery/screens/detail-screen/img_result/img_result.dart';
import 'package:cookery/screens/detail-screen/img_material/img_material.dart';
import 'package:cookery/screens/detail-screen/tab/tab_main.dart';
class RecipePage extends StatefulWidget{
  @override
  _RecipePageState createState() => _RecipePageState();
}

class _RecipePageState extends State<RecipePage>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: Color.fromRGBO(211, 211, 211, 1.0),
            child: Stack(
              children: <Widget>[
                CustomScrollView(
                    slivers: <Widget>[
                      SliverList(
                        delegate: SliverChildListDelegate(
                          [
                            ImageResult(),
                            Container(
                              height: 0.5,
                            ),
                            Description(),
                            Container(
                              height: 6,
                            ),
                            TabMain(),
                            Container(
                              height: 0.5,
                            ),
                            ImgMaterial(),
                          ],
                        ),
                      ),

//                SliverList(
//                  delegate: SliverChildListDelegate(
//                    [
//                      Stack(
//                        children: <Widget>[
//                          Container(
//                            color: Colors.white,
//                            padding: const EdgeInsets.all(5.0),
//                            child: Text('Hình ảnh nguyên liệu', style: new TextStyle(
//                              fontSize: 15.0,
//                              color: Colors.black,
//                              fontWeight: FontWeight.bold,
//                            ),
//                            ),
//                          ),
//                          ImgMaterial()
//                        ],
//                      )
//                    ],
//                  ),
//                ),
//
//                SliverList(
//                  delegate: SliverChildListDelegate(
//                    [
//                      Stack(
//                        children: <Widget>[
//                          Description()
//                        ],
//                      )
//                    ],
//                  ),
//                ),
                    ]
                ),
                CookeryBackButton(),
              ],
            )
        )
    );

  }

//@override
//  Widget build(BuildContext context) {
//
//    return Scaffold(
//      body:  ListView(
//          children: <Widget>[
//            Row(
//              children: <Widget>[
//                ImageResult(),
//                Description(),
//                //ImgMaterial(),
//              ],
//            )
//          ],
//        ),
//    );
//  }


//    @override
//  Widget build(BuildContext context) {
//
//      return Container(
//        child: Stack(
//          children: <Widget>[
//            Container(
//                    padding: const EdgeInsets.all(5.0),
//                    child: Text('Hình ảnh nguyên liệu', style: new TextStyle(
//                      fontSize: 12.0,
//                      color: Colors.black,
//                      fontWeight: FontWeight.bold,
//                    ),
//                    ),
//                  ),
//            ImageResult(),
//            ImgMaterial()
//          ],
//        ),
//      );
//  }
}