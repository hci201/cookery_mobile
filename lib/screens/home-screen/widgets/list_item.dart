import 'package:flutter/material.dart';
//import 'package:cookery/screens/home-screen/models/DetailPageModel.dart';
import 'package:cookery/screens/home-screen/models/SeeMoreModel.dart';

class ListItem extends StatelessWidget {
  final String title;
  const ListItem({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              blurRadius: 16,
              color: Colors.grey[400],
            ),
          ],
          borderRadius: BorderRadius.circular(5)),
      child: title ==  LoadingIndicatorTitle ? CircularProgressIndicator() : Text(title),
      alignment: Alignment.center,
    );
  }
}
