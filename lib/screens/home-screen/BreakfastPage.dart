import 'package:cookery/screens/back_arrow/back_arrow.dart';
import 'package:cookery/screens/home-screen/listitems/RecipeListItem.dart';
import 'package:flutter/material.dart';
import 'dart:io';

import 'package:cookery/screens/home-screen/components/Detail2Page.dart';


class BreakfastPage extends StatelessWidget {

  List images = getImageList();
  List foodNames = getFoodNameList();
  List timeNames = getTimeList();
  List detailNames = getDetailList();

  @override
  Widget build(BuildContext context) {
    /*return Container(
      height: 120.0,
      margin: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
      child: new Stack(
        children: <Widget>[
          chooseRecipe
        ],
      ),
    );*/
    /*return Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(height: 30.0),
          _buildHeader(context),
          SizedBox(height: 30.0),
          Container(

          ),
        ],
      ),
    );*/
    return Scaffold(
      backgroundColor: HexColor("#F9FAF5"),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              _builderHeader(context),
              Expanded(
                  child: Container(
                    child: ListView.builder(
                      padding: EdgeInsets.only(bottom: 30, top: 10),
                      physics: BouncingScrollPhysics(),
                      itemCount: 10,
                      itemBuilder: (BuildContext context, int index){
                        return RecipeItem(foodNames[index],images[index],timeNames[index],detailNames[index]);
                      },
                    ),
                  ),
              ),
            ],
          ),
          CookeryBackButton(),
        ],
      ),
    );
  }

  Widget _buildItem(BuildContext context, index, {bool large = false}) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (_) => Detail2Page()));
      },
      child: Container(
          height: 120.0,
          margin: const EdgeInsets.symmetric(
            vertical: 16.0,
            horizontal: 24.0,
          ),
          child: new Stack(
            children: <Widget>[
//              planetCard
              Container(
                margin: new EdgeInsets.symmetric(vertical: 16.0),
                alignment: FractionalOffset.centerLeft,
                child: new Image(
                  image: new AssetImage('images/suggested/i5.jpg'),
                  height: 92.0,
                  width: 92.0,
                ),
              ),
            ],
          )),
    );
  }

  Widget _builderHeader(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      decoration: BoxDecoration(
        color: HexColor("DC0000"),
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(40),
            bottomRight: Radius.circular(40)),
      ),
      child: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: Center(
              child: Text(
                "Bữa sáng",
                style: TextStyle(fontSize: 20,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Montserrat',
                    color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
  static List getImageList() {
    List<String> list = List.generate(10, (i) {
      return "images/suggested/transparents/breakfast${i + 1}.png";
    });
    return list;
  }

  static List getFoodNameList() {
    List<String> list = [
      'Ốp La Kèm Bánh',
      'Bánh kép',
      'Spata',
      'Cơm Tấm',
      'Bò bít tết',
      'Bánh mì',
      'Súp xào',
      'Bánh Mỳ Trứng',
      'Phô mai trái cây',
      'Trái cây'
    ];
    return list;
  }

  static List getTimeList() {
    List<String> list = [
      '20\'','15\'','30\'','20\'','20\'','10\'','25\'','30\'','22\'','20\'',
    ];
    return list;
  }

  static List getDetailList() {
    List<String> list = [
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'
    ];
    return list;
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

