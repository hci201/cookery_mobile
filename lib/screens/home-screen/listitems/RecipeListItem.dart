import 'dart:math';

import 'package:flutter/material.dart';

import 'package:smooth_star_rating/smooth_star_rating.dart';

class RecipeItem extends StatelessWidget {
  final String name;
  final String image;
  final String time;
  final String detail;
  RecipeItem(this.name, this.image, this.time, this.detail);

  @override
  Widget build(BuildContext context) {


    final chooseRecipe = Container(
      height: 150.0,
      margin: const EdgeInsets.only(left: 60.0),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: HexColor("#F9FAF5"),
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: <BoxShadow>[
            new BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0,10.0),
                blurRadius: 10.0
            )
          ]
      ),
      child: Container(
        height: 120,
        child: Padding(
          padding: EdgeInsets.fromLTRB(85, 20, 15, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                name,
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                ),
              ),
              Stack(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 180,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.access_time,
                            size: 15,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 5),
                            child: Text(
                              time,
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.w300,
                                fontSize: 12,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 5,
                    right: 0,
                    child: Padding(
                      padding: EdgeInsets.only(
                        bottom: 4,
                      ),
                      child: SmoothStarRating(
                        allowHalfRating: false,
                        starCount: 5,
                        rating: 5 - 2 * Random().nextDouble(),
                        size: 10.0,
                        color: Color(0XFFFFA127),
                        borderColor: Color(0XFFFFA127),
                        spacing: 0.0,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 2),
                child: Container(
                  width: MediaQuery.of(context).size.width - 180,
                  child: Text(
                    detail,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w300,
                      fontSize: 14,
                      color: Colors.grey[500]
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );

    final chooseImage = Container(
      margin: EdgeInsets.symmetric(vertical: 2.0),
      alignment: FractionalOffset.centerLeft,
      child: new Image(image: AssetImage(image)),
      height: 150.0,
      width: 140.0,
    );

    return Container(
            margin: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
            child: Stack(
              children: <Widget>[
                chooseRecipe,
                chooseImage,
              ],
            ),
          );

  }
}
class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
