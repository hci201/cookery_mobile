import 'package:cookery/services/scanner_service.dart';
import 'package:flutter/material.dart';

class popularSearchListItem extends StatefulWidget {
  @override
  _popularSearchListItemState createState() => _popularSearchListItemState();
}

class _popularSearchListItemState extends State<popularSearchListItem> {
  List keywords = getList();
  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: keywords.length,
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                ScannerService.searchValue = keywords[index];
                ScannerService.isAnalysing = false;
                Navigator.pushNamed(context, '/search-result');
              },
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                          child: Text(
                            keywords[index],
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w300,
                              color: Colors.black,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          color: Colors.grey.withOpacity(0.3),
                          height: 0.5,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          },
        ));
  }
  static List getList() {
    List<String> list = [
      'Bánh canh',
      'Bánh cuốn',
      'Cơm cuộn',
      'Thịt nướng',
      'Salad',
      'Phở',
      'Bún đậu mắm tôm',
      'Cháo lòng',
      'Súp cua',
      'Thịt kho tàu',
      'Canh chua cá lóc'
    ];
    return list;
  }
}
