import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

//my own imports
import 'package:cookery/screens/home-screen/components/horizonttal_listview_recipes.dart';

import 'package:cookery/screens/home-screen/components/suggestedFood.dart';
import 'package:cookery/screens/home-screen/components/HorizontalSuggested.dart';
import 'package:cookery/screens/home-screen/components/topThreeScreen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static final double _maxSizeFactor = 1.5; // image max size will 130%
  static final double _transformSpeed = 0.001; // 0.1 very fast,   0.001 slow

  ScrollController _controller;
  double _factor = 1;


  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
  }

  _scrollListener() {
    if (_controller.offset < 0) {
      _factor = 1 + _controller.offset.abs() * _transformSpeed;
      _factor = _factor.clamp(1, _maxSizeFactor);
      setState(() {});
    } else {
      if (_factor != 1) {
        _factor = 1;
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: CustomScrollView(
          physics: BouncingScrollPhysics(),
          controller: _controller,
          slivers: <Widget>[
            SliverPersistentHeader(
              floating: false,
              pinned: true,
              delegate: _SliverAppBarDelegate(
                minHeight: 0,
                maxHeight: 230,
                child: Transform.scale(
                  scale: _factor,
                  child: Container(
                    height: 200.0,
                    child: Carousel(
                      boxFit: BoxFit.cover,
                      images: [
                        AssetImage('images/suggested/i5.jpg'),
                        AssetImage('images/suggested/i6.jpg'),
                        AssetImage('images/suggested/i7.jpg'),
                        AssetImage('images/suggested/i8.jpg'),
                      ],
                      autoplay: true,
                      animationCurve: Curves.fastOutSlowIn,
                      animationDuration: Duration(milliseconds: 1000),
                      dotSize: 4.0,
                    ),
                  ),
                ),
              ),
            ),
//            SliverList(
//              delegate: SliverChildListDelegate(
//                [
//                  Container(
//                    child: GestureDetector(
//                      onTap: () {
//                        setState(() {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (_) => SearchScreen()));
//                        });
//                      },
//                      child: Container(
//                        margin: EdgeInsets.all(10.0),
//                        padding: EdgeInsets.all(10.0),
//                        decoration: myBoxDecoration(),
//                        child: Row(
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          children: <Widget>[
//                            Padding(
//                              padding: EdgeInsets.only(right: 10),
//                              child: Icon(Icons.search, size: 20, color: Colors.grey,),
//                            ),
//                            Text(
//                              "Tìm kiếm thực đơn, món ăn, nguyên liệu",
//                              maxLines: 1,
//                              overflow: TextOverflow.ellipsis,
//                              style: TextStyle(
//                                  fontSize: 12.0,
//                                  fontFamily: "Montserrat",
//                                  fontWeight: FontWeight.w500,
//                                  color: Colors.grey),
//                            ),
//                          ],
//                        ),
//                      ),
//                    ),
//                  )
//                  /*child: FocusScope(
//                      child: Focus(
//                        onFocusChange: (_) => Navigator.push(context, MaterialPageRoute(builder: (_) => SearchScreen())),
//                        child: TextFormField(
//                          decoration: new InputDecoration(
//                            contentPadding: EdgeInsets.all(10.0),
//                            icon: Image.asset('images/icons/icon-search-black.png'),
//                            labelText: "Tìm kiếm thực đơn, món ăn, nguyên liệu",
//                            fillColor: Colors.white,
//                            border: new OutlineInputBorder(
//                              borderRadius: new BorderRadius.circular(25.0),
//                              borderSide: new BorderSide(),
//                            ),
//                          ),
//                          style: new TextStyle(
//                            fontFamily: "Montserrat-Medium",
//                          ),
//                        ),
//                      ),
//                    ),*/
//                  /*child: TextField(
//                      decoration: new InputDecoration(
//                        contentPadding: EdgeInsets.all(10.0),
//                        icon: Image.asset('images/icons/icon-search-black.png'),
//                        labelText: "Tìm kiếm thực đơn, món ăn, nguyên liệu",
//                        fillColor: Colors.white,
//                        border: new OutlineInputBorder(
//                          borderRadius: new BorderRadius.circular(25.0),
//                          borderSide: new BorderSide(),
//                        ),
//                      ),
//                      style: new TextStyle(
//                        fontFamily: "Montserrat-Medium",
//                      ),
//                    ),*/
//                ],
//              ),
//            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  GestureDetector(
                    onTap: () => Navigator.pushNamed(context, '/search'),
                    child: Container(
                      padding: EdgeInsets.all(15),
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          border: Border.all(width: 0.7, color: Colors.grey),
                          borderRadius: BorderRadius.all(
                              Radius.circular(20.0) //         <--- border radius here
                              ),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.search, size: 21, color: Colors.grey),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                'Tìm kiếm món ăn, nguyên liệu',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  fontFamily: "Montserrat",
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black45,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ]
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [

                  TopThreeList(),
                ]
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  //padding widget
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0),
                    child: Text("Phân loại thực đơn",
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Color(0XFF2A2A2A))),
                  ),

                  //Horizontal list view
                  HorizontalRecipeList(),
                ],
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  Stack(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                        child: Text('Món ngon tuần này',
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color(0XFF2A2A2A))),
                      ),
                      Container(
                        alignment: const Alignment(1.05, -1.0),
                        padding: const EdgeInsets.only(left:10.0, right:25.0, top: 15.0, bottom: 10.0),
                        child: GestureDetector(
                          onTap: () => Navigator.pushNamed(context, '/detail'),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Xem thêm',
                                  style: TextStyle(
                                    decoration: TextDecoration.underline,
                                      color: Colors.red,
                                      fontFamily: 'Montserrat',
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  //horizontal view
                  Container(
                    height: 180.0,
                    //child: SuggestedDishes(),
                    child: HorizontalSuggested(),
                  ),
                ],
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  Stack(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                        child: Text('Được yêu thích nhất',
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color(0XFF2A2A2A))),
                      ),
                      Container(
                        alignment: const Alignment(1.05, -1.0),
                        padding: const EdgeInsets.only(left:10.0, right:25.0, top: 15.0, bottom: 10.0),
                        child: GestureDetector(
                          onTap: () => Navigator.pushNamed(context, '/detail'),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Xem thêm',
                                  style: TextStyle(
                                    decoration: TextDecoration.underline,
                                      color: Colors.red,
                                      fontFamily: 'Montserrat',
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  //horizontal view
                  Container(
                    height: 180.0,
                    //child: SuggestedDishes(),
                    child: HorizontalSuggested(),
                  ),
                ],
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  Stack(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(left: 20.0, right: 100.0, top: 10.0, bottom: 10.0),
                        child: Text('Bởi vì bạn thích:\nMón chay',
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color(0XFF2A2A2A))),
                      ),
                      Container(
                        alignment: const Alignment(1.05, -1.0),
                        padding: const EdgeInsets.only(left:10.0, right:25.0, top: 15.0, bottom: 10.0),
                        child: GestureDetector(
                          onTap: () => Navigator.pushNamed(context, '/detail'),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Xem thêm',
                                  style: TextStyle(
                                    decoration: TextDecoration.underline,
                                      color: Colors.red,
                                      fontFamily: 'Montserrat',
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  //horizontal view
                  Container(
                    height: 180.0,
                    //child: SuggestedDishes(),
                    child: HorizontalSuggested(),
                  ),
                ],
              ),
            ),
            SliverList(
              /*gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),*/
              delegate: SliverChildListDelegate(
                [
                  Stack(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                        child: Text(
                          'Có thể bạn sẽ thích',
                          style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Color(0XFF2A2A2A)),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      SuggestedFoodScreen(),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: Center(
                          child: Container(
                            width: 25,
                            height: 25,
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  //SuggestedFoodScreen()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(width: 0.7, color: Colors.grey),
    borderRadius: BorderRadius.all(
        Radius.circular(20.0) //         <--- border radius here
        ),
  );
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => maxHeight;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight || minHeight != oldDelegate.minHeight || child != oldDelegate.child;
  }
}

