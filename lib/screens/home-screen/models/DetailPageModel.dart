import 'package:flutter/widgets.dart';

const String LoadingIndicatorTitle = '^';

class DetailPageModel extends ChangeNotifier {
  static const int ItemRequestThreshold = 10;

  int _currentPage = 0;
  List<String> _items;
  List<String> get items => _items;

  DetailPageModel() {
    _items = List<String>.generate(10, (index) => 'Title $index');
  }

  Future handleItemCreated(int index) async {
    var itemPosition = index + 1;
    var requestMoreData = itemPosition % ItemRequestThreshold == 0;

    // ~/ divide and truncate
    var pageToRequest = itemPosition ~/ ItemRequestThreshold;

    if(requestMoreData && pageToRequest > _currentPage){
      print('handleItemCreated | pageToRequest: $pageToRequest');
      _currentPage = pageToRequest;
      _showLoadingIndicator();

      await Future.delayed(Duration(seconds: 3));

      var newFetchedItems = List<String>.generate(
          10, (index) => 'Title page: $_currentPage item:$index');

      _items.addAll(newFetchedItems);

      _removeLoadingIndicator();
    }
  }

  void _showLoadingIndicator(){
    _items.add(LoadingIndicatorTitle);
    notifyListeners();
  }

  void _removeLoadingIndicator(){
    _items.remove(LoadingIndicatorTitle);
    notifyListeners();
  }
}
