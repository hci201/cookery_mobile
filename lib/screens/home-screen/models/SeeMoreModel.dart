import 'package:flutter/widgets.dart';

const String LoadingIndicatorTitle = '^';
Food LoadingIndicatorFood = new Food('Bò nướng kobe', 'images/suggested/i10.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt');

class SeeMoreModel extends ChangeNotifier {
  static const int ItemRequestThreshold = 10;

  int _currentPage = 0;
  bool _isDisposed = false;
  List<String> _items;
  List<String> get items => _items;

  List<Food> _foodItems;
  List<Food> get foodItems => _foodItems;

  SeeMoreModel() {
//    _items = List<String>.generate(10, (index) => 'Title $index');
      _foodItems = [
        Food('Hột vịt sữa', 'images/suggested/i1.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'),
        Food('Hột vịt lộn', 'images/suggested/i2.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'),
        Food('Trứng cút lộn', 'images/suggested/i3.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'),
        Food('Bắp xào', 'images/suggested/i4.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'),
        Food('Trứng cút xào me', 'images/suggested/i5.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'),
        Food('Trứng cút xào nước mắm', 'images/suggested/i6.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'),
        Food('Gà nướng', 'images/suggested/i7.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'),
        Food('Gà luộc', 'images/suggested/i8.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'),
        Food('Bò xào', 'images/suggested/i9.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'),
        Food('Bò nướng kobe', 'images/suggested/i10.jpg', '15p', 'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt')
      ];
  }

  @override
  void dispose() { 
    this._isDisposed = true;
    super.dispose();
  }

  Future handleItemCreated(int index) async {
    var itemPosition = index + 1;
    var requestMoreData = itemPosition % ItemRequestThreshold == 0;

    // ~/ divide and truncate
    var pageToRequest = itemPosition ~/ ItemRequestThreshold;

    if(requestMoreData && pageToRequest > _currentPage){
      print('handleItemCreated | pageToRequest: $pageToRequest');
      _currentPage = pageToRequest;
      _showLoadingIndicator();

      await Future.delayed(Duration(seconds: 3));

//      var newFetchedItems = List<String>.generate(
//          10, (index) => 'Title page: $_currentPage item:$index');
      var newFetchedItems = List<Food>.generate(_foodItems.length, (index) => _foodItems[index]);

//      _items.addAll(newFetchedItems);
      _foodItems.addAll(newFetchedItems);
      _removeLoadingIndicator();
    }
  }

  void _showLoadingIndicator(){
//    _items.add(LoadingIndicatorTitle);
  _foodItems.add(LoadingIndicatorFood);
    if (!_isDisposed) {
      notifyListeners();
    }
  }

  void _removeLoadingIndicator(){
//    _items.remove(LoadingIndicatorTitle);
  _foodItems.remove(LoadingIndicatorFood);
    if (!_isDisposed) {
      notifyListeners();
    }
  }
}

class Food {
  final String foodName;
  final String foodImage;
  final String material;
  final String detail;

  Food(this.foodName, this.foodImage, this.material, this.detail);
}
