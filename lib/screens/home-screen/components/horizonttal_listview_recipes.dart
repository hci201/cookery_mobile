import 'package:flutter/material.dart';

import 'package:cookery/screens/home-screen/BreakfastPage.dart';

class HorizontalRecipeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20.0, right: 20, left: 10),
      height: 150.0,
      width: 100.0,
      child: ListView(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Recipe(
            image_location: 'images/recipes/breakfast.png',
            image_caption: 'bữa sáng',
          ),Recipe(
            image_location: 'images/recipes/lunch-box.png',
            image_caption: 'bữa trưa',
          ),Recipe(
            image_location: 'images/recipes/dinner.png',
            image_caption: 'bữa tối',
          ),Recipe(
            image_location: 'images/recipes/cake.png',
            image_caption: 'bánh ngọt',
          ),Recipe(
            image_location: 'images/recipes/fruit.png',
            image_caption: 'tráng miệng',
          ),Recipe(
            image_location: 'images/recipes/drink.png',
            image_caption: 'thức uống',
          ),Recipe(
            image_location: 'images/recipes/diet.png',
            image_caption: 'giảm cân',
          ),Recipe(
            image_location: 'images/recipes/meat.png',
            image_caption: 'tăng cân',
          ),Recipe(
            image_location: 'images/recipes/fastfood.png',
            image_caption: 'thức ăn nhanh',
          ),
        ],
      ),
    );
  }
}

class Recipe extends StatelessWidget {
  final String image_location;
  final String image_caption;

  Recipe({this.image_location, this.image_caption});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: InkWell(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => BreakfastPage() ));
        },
        child: Container(
          padding: EdgeInsets.only(left: 20, right: 10),
          child: Column(
            children: <Widget>[
              Image.asset(
                image_location,
                height: 70.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 8),
                child: Text(image_caption, style: new TextStyle(fontSize: 12.0),),
              ),
            ],
          ),
//          ListTile(
//            title: Image.asset(
//              image_location,
//              width: 100.0,
//              height: 80.0,
//            ),
//            subtitle: Container(
//              alignment: Alignment.topCenter,
//              child: Text(image_caption, style: new TextStyle(fontSize: 12.0),),
//            ),
//          ),
        ),
      ),
    );
  }
}
