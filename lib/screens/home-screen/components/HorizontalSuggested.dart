import 'package:flutter/material.dart';

class HorizontalSuggested extends StatefulWidget {
  @override
  _HorizontalSuggestedState createState() => _HorizontalSuggestedState();
}

class _HorizontalSuggestedState extends State<HorizontalSuggested> {
  Widget mySuggested(String imageVal, String heading, String subHeading, String time, String seen) {
    return Container(
      margin: EdgeInsets.only(right: 20, top: 5, bottom: 5),
      width: 180.0,
        child: GestureDetector(
          onTap: () => Navigator.pushNamed(context, '/recipe2'),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[300],
                  blurRadius: 3,
                  offset: Offset(0.5, 0.8),
                ),
              ],
              borderRadius: BorderRadius.circular(3),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  height: 98.0,
                  width: 180.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(3), topRight: Radius.circular(3)),
                    child: Image(
                      image: new AssetImage(imageVal),
                      height: 100.0,
                      width: 180.0,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10, top: 6),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(heading,
                                style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'Montserrat',
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 5),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(
                                      Icons.alarm_on,
                                      color: Colors.grey,
                                      size: 15,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 3),
                                      child: Text(time,
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w400
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),

                        ],
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      right: 10,
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 3),
                            child: Text(seen,
                              style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.w400
                              ),
                            ),
                          ),
                          Icon(Icons.favorite, size: 15, color: Colors.red),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new ListView(
        physics: NeverScrollableScrollPhysics(),
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
        children: <Widget>[
          new Container(
//            margin: EdgeInsets.symmetric(vertical: 10.0),
            height: 160.0,
            child: new ListView(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                mySuggested("images/suggested/myquang.jpg", "Mỳ Quảng",
                    "Đặc sản Quảng Nam","15p","22.1k"),
                mySuggested("images/suggested/bundaumamtom.jpg", "Bún Đậu",
                    "Đặc sản Quảng Nam","25p","19.2k"),
                mySuggested("images/suggested/comgatamky.jpg", "Cơm Gà",
                    "Đặc sản Quảng Nam","10p","12.8k"),
                mySuggested("images/suggested/banhdap.jpg", "Bánh Đập",
                    "Đặc sản Quảng Nam","20p","11.2k"),
                mySuggested("images/suggested/banhxeo.jpg", "Bánh Xèo",
                    "Đặc sản Quảng Nam","20p","9.7k"),
                mySuggested("images/suggested/bethui.jpg", "Bê Thui",
                    "Đặc sản Quảng Nam","10p","7.5k"),
              ],
            ),
          ),
          new SizedBox(
            height: 10.0,
          )
        ],
      ),
    );
  }
}
