import 'dart:math';

import 'package:cookery/screens/back_arrow/back_arrow.dart';
import 'package:cookery/screens/detail-screen-clone/detail_page2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:cookery/screens/home-screen/listitems/creation_aware_list_item.dart';
import 'package:provider/provider.dart';
import 'package:cookery/screens/home-screen/models/SeeMoreModel.dart';

import 'package:smooth_star_rating/smooth_star_rating.dart';
class DetailPage extends StatelessWidget {
  /*const DetailPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Detail Page"),
        backgroundColor: Colors.red,
      ),
      body: ChangeNotifierProvider<DetailPageModel>(
        builder: (context) => DetailPageModel(),
        child: Consumer<DetailPageModel>(
          builder: (context, model, child) => ListView.builder(
            itemCount: model.items.length,
            itemBuilder: (context, index) => CreationAwareListItem(
              itemCreated: () {
                SchedulerBinding.instance.addPostFrameCallback(
                        (duration) => model.handleItemCreated(index));
              },
              child: ListItem(
                title: model.items[index],
              ),
            )),
        ),
      ),
    );
  }*/
  const DetailPage({Key key}) : super(key: key);

  Widget _builderHeader(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 30),
          child: Center(
            child: Text(
              "Món ngon tuần này",
              style: TextStyle(fontSize: 20,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Montserrat',
                  color: Colors.white),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          ChangeNotifierProvider<SeeMoreModel>(
            builder: (context) => SeeMoreModel(),
            child: Consumer<SeeMoreModel>(
              builder: (context, model, child) =>
                  ListView.builder(
                    physics: BouncingScrollPhysics(),
                      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 15.0, bottom: 15.0),
                      itemCount: model.foodItems.length + 1,
                      itemBuilder: (context, index) => index != model.foodItems.length ?
                          Padding(
                            padding: index == 0 ? EdgeInsets.only(bottom: 20, top: 100) : EdgeInsets.only(bottom: 20),
                            child: CreationAwareListItem(
                                itemCreated: () {
                                  SchedulerBinding.instance.addPostFrameCallback(
                                          (duration) =>
                                          model.handleItemCreated(index));
                                },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey[300],
                                          blurRadius: 3,
                                          offset: Offset(0.5, 0.8),
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(3),
                                    ),
                                    child: Container(
                                      child: GestureDetector(
                                        onTap: (){
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (_)=>
                                                      RecipePage2(),
                                              )
                                          );
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.only(left: 2),
                                              child: Container(
                                                height: 116.0,
                                                width: 116.0,
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.only(
                                                        bottomLeft: Radius.circular(3),
                                                        topLeft: Radius.circular(3)
                                                    ),
                                                    image: DecorationImage(
                                                        fit: BoxFit.cover,
                                                        image: AssetImage(model.foodItems[index].foodImage)
                                                    )
                                                ),
                                              ),
                                            ),
                                            Container(
                                              height: 120,
                                              child: Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    10, 2, 0, 0),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment
                                                      .start,
                                                  children: <Widget>[
                                                    Text(
                                                      model.foodItems[index].foodName,
                                                      style: TextStyle(
                                                        fontFamily: 'Montserrat',
                                                        fontWeight: FontWeight.w600,
                                                        fontSize: 16,
                                                      ),
                                                    ),
                                                    Stack(
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                                                          child: Container(
                                                            width: MediaQuery.of(context).size.width - 180,
                                                            child: Row(
                                                              children: <Widget>[
                                                                Icon(
                                                                  Icons.access_time,
                                                                  size: 15,
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: 5),
                                                                  child: Text(
                                                                    model.foodItems[index].material,
                                                                    style: TextStyle(
                                                                      fontFamily: 'Montserrat',
                                                                      fontWeight: FontWeight.w300,
                                                                      fontSize: 12,
                                                                      color: Colors.black,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        Positioned(
                                                          top: 5,
                                                          right: 0,
                                                          child: Padding(
                                                            padding: EdgeInsets.only(
                                                              bottom: 4,
                                                            ),
                                                            child: SmoothStarRating(
                                                              allowHalfRating: false,
                                                              starCount: 5,
                                                              rating: 5 - 2 * Random().nextDouble(),
                                                              size: 10.0,
                                                              color: Color(0XFFFFA127),
                                                              borderColor: Color(0XFFFFA127),
                                                              spacing: 0.0,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.fromLTRB(
                                                          0, 5, 0, 2),
                                                      child: Container(
                                                        width: MediaQuery.of(context).size.width - 180,
                                                        child: Text(model.foodItems[index].detail,
                                                          maxLines: 3,
                                                          overflow: TextOverflow.ellipsis,
                                                          textAlign: TextAlign.justify,
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontWeight: FontWeight.w300,
                                                            fontSize: 14,
                                                            color: Colors.grey[500]
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                ),
                            ),
                          ) : Padding(
                            padding: EdgeInsets.only(top: 20, bottom: 20),
                            child: Center(
                              child: Container(
                                width: 25,
                                height: 25,
                                child: CircularProgressIndicator(
                                  strokeWidth: 2,
                                ),
                              ),
                            ),
                          )),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 100,
            decoration: BoxDecoration(
              color: Color(0XFFDC0000),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(40),
                  bottomRight: Radius.circular(40)),
            ),
            child: _builderHeader(context),
          ),
          CookeryBackButton(),
        ],
      ),
    );
  }
}
