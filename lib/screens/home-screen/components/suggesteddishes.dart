import 'package:flutter/material.dart';

class SuggestedDishes extends StatefulWidget {
  @override
  _SuggestedDishesState createState() => _SuggestedDishesState();
}

class _SuggestedDishesState extends State<SuggestedDishes> {
  var suggested_dishes_list = [
    {
      "name":"Mỳ Quảng",
      "picture":"images/suggested/myquang.jpg",
      "type":"Đặc sản Quảng Nam",
      "calo":"420 Calo",
    },
    {
      "name":"Mỳ Quảng 1",
      "picture":"images/suggested/myquang.jpg",
      "type":"Đặc sản Quảng Nam",
      "calo":"420 Calo",
    },
    {
      "name":"Mỳ Quảng 2",
      "picture":"images/suggested/myquang.jpg",
      "type":"Đặc sản Quảng Nam",
      "calo":"420 Calo",
    },
    {
      "name":"Mỳ Quảng 3",
      "picture":"images/suggested/myquang.jpg",
      "type":"Đặc sản Quảng Nam",
      "calo":"420 Calo",
    },
    {
      "name":"Bún Đậu Mắm Tôm",
      "picture":"images/suggested/bundaumamtom.jpg",
      "type":"Món ăn ưa chuộng",
      "calo":"560 Calo",
    },
    {
      "name":"Cơm Gà Tam Kỳ",
      "picture":"images/suggested/comgatamky.jpg",
      "type":"Đặc sản Quảng Nam",
      "calo":"300 Calo",
    },
    {
      "name":"Cơm Gà Tam Kỳ",
      "picture":"images/suggested/comgatamky.jpg",
      "type":"Đặc sản Quảng Nam",
      "calo":"300 Calo",
    },
    {
      "name":"Cơm Gà Tam Kỳ",
      "picture":"images/suggested/comgatamky.jpg",
      "type":"Đặc sản Quảng Nam",
      "calo":"300 Calo",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: GridView.builder(
          itemCount: suggested_dishes_list.length,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          itemBuilder: (BuildContext context, int index){
            return Single_dish(
              dish_name: suggested_dishes_list[index]['name'],
              dish_picture: suggested_dishes_list[index]['picture'],
              dish_type: suggested_dishes_list[index]['type'],
              dish_calo: suggested_dishes_list[index]['calo'],
            );
          }),
    );
  }
}

class Single_dish extends StatelessWidget {
  final dish_name;
  final dish_picture;
  final dish_type;
  final dish_calo;

  Single_dish({
    this.dish_name,
    this.dish_picture,
    this.dish_type,
    this.dish_calo,
});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
        tag: dish_name,
        child: Material(
          child: InkWell(onTap: (){},
          child: GridTile(
            footer: Container(
              color: Colors.white70,
              child: ListTile(
                leading: Text(
                  dish_name,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                title: Text(
                  dish_type,
                  style: TextStyle(
                      color: Colors.red, fontWeight: FontWeight.w900),
                ),
                subtitle: Text(
                  dish_calo,
                  style: TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.w800),
                ),
              ),
            ),
            child: Image.asset(dish_picture,
            fit: BoxFit.cover,)),
          ),
        )),
    );
  }
}


