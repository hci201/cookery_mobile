import 'package:cookery/screens/home-screen/BreakfastPage.dart';
import 'package:flutter/material.dart';


class TopThreeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 460,
      padding: EdgeInsets.only(top: 15.0),
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.only(left: 20, right: 20),
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Món mới cập nhật",
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Color(0XFF2A2A2A))),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            child: Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => Navigator.pushNamed(context, '/recipe2'),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Container(
                          width: (MediaQuery.of(context).size.width - 50) / 2 + 30,
                          height: MediaQuery.of(context).size.width - 60,
                          padding: EdgeInsets.all(12),
                          color: HexColor("#C60808"),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 50,
                              ),
                              Image.asset(
                                  'images/suggested/transparents/Cheeseantipasti.png'),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                "Phô mai đào mật ong",
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    color: Colors.white),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Stack(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Icon(
                                              Icons.alarm_on,
                                              size: 14,
                                              color: Colors.white,
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(left: 3),
                                              child: Text("15p",
                                              style: TextStyle(color: Colors.white, fontFamily: 'Montserrat', fontSize: 14.0, fontWeight: FontWeight.w300),),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  Positioned(
                                    top: 5,
                                    right: 0,
                                    child: Text(
                                      "Khai vị Ý",
                                      style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w300,
                                        fontSize: 12,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                              SizedBox(
                                height: 12,
                              ),
                              Container(
                                width: (MediaQuery.of(context).size.width - 50) / 2 + 6,
                                child: Text(
                                  "Một trong những món ăn phổ biến thường có trong thực đơn hằng ngày của người dân nước Ý và một số nước châu Âu.",
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 8,
                                    color: Colors.white,
                                  ),
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  width: 10,
                ),
                Column(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        width: (MediaQuery.of(context).size.width - 50) / 2 - 30,
                        height: (MediaQuery.of(context).size.width - 50) / 2 - 10,
                        padding: EdgeInsets.all(12),
                        color: Color(0XFFEEF5DB),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            Image.asset(
                              'images/suggested/transparents/menu-1-yoshimasa-sushi.png',
                              height: 65,
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              "Yoshimasa Sushi",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Stack(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.alarm_on,
                                      size: 12,
                                      color: Colors.black,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text("30p",
                                        style: TextStyle(color: Colors.black, fontFamily: 'Montserrat', fontSize: 11.0, fontWeight: FontWeight.w300),),
                                    ),
                                  ],
                                ),
                                Positioned(
                                  top: 1,
                                  right: 0,
                                  child: Text(
                                    "Món Nhật",
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w300,
                                      fontSize: 10,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        width: (MediaQuery.of(context).size.width - 50) / 2 - 30,
                        height: (MediaQuery.of(context).size.width - 50) / 2 - 10,
                        color: HexColor("#333745"),
                        padding: EdgeInsets.all(12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            Image.asset(
                              'images/suggested/transparents/menu-3-prato-sushi.png',
                              height: 65,
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              "Prato Kimbap",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 13,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Stack(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.alarm_on,
                                      size: 12,
                                      color: Colors.white,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 3),
                                      child: Text("20p",
                                        style: TextStyle(color: Colors.white, fontFamily: 'Montserrat', fontSize: 11.0, fontWeight: FontWeight.w300),),
                                    ),
                                  ],
                                ),
                                Positioned(
                                  top: 1,
                                  right: 0,
                                  child: Text(
                                    "Món Hàn",
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w300,
                                      fontSize: 10,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
