import 'package:cookery/screens/back_arrow/back_arrow.dart';
import 'package:cookery/services/scanner_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tags/tag.dart';
import 'package:cookery/screens/home-screen/listitems/popularListItem.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final FocusNode _searchFocus = FocusNode();
  final List<String> _items = [
    'điểm tâm sáng',
    'món ăn miền trung',
    'món tráng miệng',
    'bánh ngọt',
    'low-carb',
    'món nhật',
    'món hàn',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 85),
            child: CustomScrollView(
              physics: BouncingScrollPhysics(),
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      Container(
                        padding: EdgeInsets.only(
                            top: 25, left: 20, right: 20, bottom: 25),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        right: 0,
                                        bottom: 5.1,
                                        child: Container(
                                          width: MediaQuery.of(context).size.width,
                                          height: 1,
                                          color: Color(0XFF707070),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(right: 10),
                                        color: Color(0XFFF9FAF5),
                                        child: Text(
                                          "Tìm kiếm gần đây",
                                          style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600,
                                            color: Color(0XFF2A2A2A),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate([
                    _tags1,
                  ]),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      Container(
                        padding: EdgeInsets.only(
                            top: 25, left: 20, right: 20),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        right: 0,
                                        bottom: 5.1,
                                        child: Container(
                                          width: MediaQuery.of(context).size.width,
                                          height: 1,
                                          color: Color(0XFF707070),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(right: 10),
                                        color: Color(0XFFF9FAF5),
                                        child: Text(
                                          "Từ khóa thịnh hành",
                                          style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600,
                                            color: Color(0XFF2A2A2A),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SliverList(
                  /*delegate: SliverChildBuilderDelegate((context,index)=>
                      Card(
                        child: Container(
                          padding: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              CircleAvatar(
                                backgroundColor: Colors.transparent,
                                backgroundImage: AssetImage('images/icons/icon-search-black.png'),
                              ),
                              Text(keywords[index]),
                            ],
                          ),
                        ),
                      ),
                  ),*/
                  delegate: SliverChildListDelegate([
                    Container(
                      child: popularSearchListItem(),
                    )
                  ]),
                ),
              ],
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 100,
              decoration: BoxDecoration(
                color: HexColor("DC0000"),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(40),
                    bottomRight: Radius.circular(40)),
              ),
              child: _builderHeader(context),
            ),
          ),
          CookeryBackButton(),
        ],
      ),
    );
  }

  Widget _builderHeader(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 45, top: 45),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(_searchFocus);
            },
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: Container(
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        color: Color(0XFFF9FAF5),
                        border: Border.all(width: 0.7, color: Colors.white),
                        borderRadius: BorderRadius.all(
                            Radius.circular(20.0) //         <--- border radius here
                            ),
                      ),
                      child: Stack(
                        children: <Widget>[
                          Icon(Icons.search, size: 21, color: Colors.grey,),
                          Padding(
                            padding: EdgeInsets.only(left: 31),
                            child: TextField(
                              focusNode: _searchFocus,
                              scrollPadding: EdgeInsets.zero,
                              onSubmitted: (text) {
                                FocusScope.of(context).requestFocus(FocusNode());
                                if (text.isNotEmpty) {
                                  ScannerService.searchValue = text;
                                  ScannerService.isAnalysing = false;
                                  Navigator.pushNamed(context, '/search-result');
                                }
                              },
                              autocorrect: false,
                              cursorColor: Theme.of(context).primaryColor,
                              decoration: InputDecoration.collapsed(
                                hintText: 'Tìm kiếm món ăn, nguyên liệu',
                              ),
                              style: getTextStyle(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        // Positioned(
        //   top: 30,
        //   right: 14,
        //   child: IconButton(
        //     icon: Icon(
        //       Icons.filter_center_focus,
        //       color: Colors.white,
        //       size: 35,
        //     ),
        //     onPressed: () => Navigator.pushNamed(context, '/scanner'),
        //   ),
        // ),
      ],
    );
  }

  TextStyle getTextStyle() {
    return TextStyle(
      fontSize: 15.0,
      fontFamily: "Montserrat",
      fontWeight: FontWeight.w500,
      color: Colors.black,
    );
  }

  InputDecoration customInputDecoration(String hint) {
    return InputDecoration(
      labelText: hint,
      labelStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.all(10),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide(color: Colors.white)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: Colors.white)),
    );
  }

  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();

  Widget get _tags1 {
    return Tags(
      key: _tagStateKey,
      symmetry: false,
      columns: 4,
      horizontalScroll: false,
      //verticalDirection: VerticalDirection.up, textDirection: TextDirection.rtl,
      heightHorizontalScroll: 60 * (19 / 14),
      itemCount: _items.length,
      itemBuilder: (index) {
        final item = _items[index];

        return ItemTags(
          key: Key(index.toString()),
          index: index,
          title: item,
          pressEnabled: true,
          activeColor: Theme.of(context).primaryColor,
          elevation: 2,
          singleItem: true,
          splashColor: Colors.green,
          active: false,
          combine: ItemTagsCombine.withTextBefore,
          onPressed: (item) {
            ScannerService.searchValue = item.title;
            ScannerService.isAnalysing = false;
            Navigator.pushNamed(context, '/search-result');
          },
          removeButton: ItemTagsRemoveButton(
            backgroundColor: Colors.white,
            color: Theme.of(context).primaryColor,
          ),
          onRemoved: () {
            setState(() {
              _items.removeAt(index);
            });
          },
        );
      },
    );
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
