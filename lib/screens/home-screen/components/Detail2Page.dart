import 'package:flutter/material.dart';

class Detail2Page extends StatefulWidget {
  @override
  _Detail2PageState createState() => _Detail2PageState();
}

class _Detail2PageState extends State<Detail2Page> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Detail 2 Page"),
        backgroundColor: Colors.red,
      ),
      body: new Center(
        child: new Text("This is Detail 2 Page"),
      ),
    );
  }
}
