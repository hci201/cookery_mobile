import 'package:cookery/services/scanner_service.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CookBookListItem extends StatefulWidget {
  @override
  _CookBookListItemState createState() => _CookBookListItemState();
}

class _CookBookListItemState extends State<CookBookListItem> {
  var saved_list = [
    {
      "name":"Yêu Thích",
      "picture":"images/suggested/i1.jpg",
      "num":"12 Công thức",
    },
    {
      "name":"Bánh Ngọt",
      "picture":"images/suggested/i2.jpg",
      "num":"8 Công thức",
    },
    {
      "name":"Giảm Cân",
      "picture":"images/suggested/i3.jpg",
      "num":"23 Công thức",
    },
  ];
/*  var saveItem = {
    "name":"aaaa",
    "picture":"images/suggested/i1.jpg",
    "num":"40 Công thức",
  };*/
    Key loadKey;
    var saveItem;
    TextEditingController customController = TextEditingController();
    Future<String> _createDialog(BuildContext context){
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Nhập tên Bộ sưu tập"),
            content: TextFormField(
              controller: customController,
            ),
            actions: <Widget>[
              MaterialButton(
                elevation: 5,
                child: Text("Tạo mới"),
                onPressed: () {
                  /*print(customController.text.toString());*/
                  saveName();
                  /*saved_list.add(saveItem);*/
                },
              ),
            ],
          );
        }
      );
  }
  void saveName(){
      String name = customController.text.toString();
      Navigator.of(context).pop(name);
/*      saveNamePreference(name).then((bool committed){
        Navigator.of(context).pop(name);
      });*/
  }
/*  String _name="";*/
  @override
  void initState() {
    super.initState();
    loadKey = Key("aaa");
    /*getNamePreference().then(updateName);*/
  }

/*  void updateName(String name){
    setState(() {
      this._name = name;
    });
  }*/
  @override
  Widget build(BuildContext context) {

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      child: GridView.builder(
          padding: EdgeInsets.zero,
          physics: BouncingScrollPhysics(),
          key: loadKey,
          itemCount: saved_list.length + 1,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          itemBuilder: (BuildContext context, int index){
            return index < saved_list.length ? GestureDetector(
              onTap: () {
                ScannerService.searchValue = saved_list[index]['name'];
                ScannerService.isAnalysing = false;
                ScannerService.hideSearchResultTitle = true;
                Navigator.pushNamed(context, '/cookbook-detail');
              },
              child: SaveItem(
                saveName: saved_list[index]['name'],
                saveNum: saved_list[index]['num'],
                image: saved_list[index]['picture'],
              ),
            ) :  Container(
              padding: EdgeInsets.all(5),
              child: Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0.5, 0.8),
                      blurRadius: 3,
                      color: Colors.grey[200],
                    ),
                  ],
                ),
                child: DottedBorder(
                  dashPattern: [6, 6, 6, 6],
                  borderType: BorderType.RRect,
                  radius: Radius.circular(8),
                  child: GestureDetector(
                    onTap: () {
                      /*getNamePreference().then(updateName);*/
                      _createDialog(context).then((onValue) {
                        if(onValue!=null){
                          /*print(_name);*/
                          saveItem = {
                            "name":onValue,
                            "picture":"images/suggested/i1.jpg",
                            "num":"0 Công thức",
                          };
                          saved_list.add(saveItem);
                          setState(() {
                            loadKey = Key("bbb");
                          });
                        }
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.add, color: Colors.grey,),
                              Text("TẠO MỚI", style: (
                                  TextStyle(fontFamily: 'Montserrat',
                                      fontSize: 20,
                                      color: Colors.grey)
                              ),)
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
    );
  }
}

Future<bool> saveNamePreference(String name) async{
  SharedPreferences pref = await SharedPreferences.getInstance();
  pref.setString("name", name);
  return pref.commit();
}

Future<String> getNamePreference() async{
  SharedPreferences pref = await SharedPreferences.getInstance();
  String name = pref.getString("name");
  return name;
}

class SaveItem extends StatelessWidget {
  final String saveName;
  final String saveNum;
  final String image;

  SaveItem({
    this.saveName,
    this.saveNum,
    this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                offset: Offset(1, 2),
                blurRadius: 3,
                color: Colors.grey[300],
              ),
            ],
            image: DecorationImage(
                image: AssetImage(image),
                fit: BoxFit.cover
            )
        ),
        child: Container(
          child: Stack(
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Center(
                    child: Column(
                      children: <Widget>[
                        Text(saveName, style: TextStyle(
                            fontSize: 28,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Montserrat',
                            color: Colors.white),),
                        Text(saveNum, style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            fontFamily: 'Montserrat',
                            color: Colors.white),)
                      ],
                    ),
                  ),
                ],
              ),
              Positioned(
                top: 0,
                left: 0,
                child: Container(
                  width: (MediaQuery.of(context).size.width - 40) / 2,
                  height: (MediaQuery.of(context).size.width - 40) / 2,
                  decoration: BoxDecoration(
                    color: Colors.white38,
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

