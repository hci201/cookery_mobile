import 'package:cookery/screens/back_arrow/back_arrow.dart';
import 'package:cookery/screens/cookbook-screen/cookbook_detail_list.dart';
import 'package:cookery/services/scanner_service.dart';
import 'package:flutter/material.dart';

class CookbookDetailScreen extends StatefulWidget {
  CookbookDetailScreen({Key key}) : super(key: key);

  @override
  _CookbookDetailScreenState createState() => _CookbookDetailScreenState();
}

class _CookbookDetailScreenState extends State<CookbookDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 100),
            child: CustomScrollView(
              physics: BouncingScrollPhysics(),
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      Padding(
                        padding: EdgeInsets.only(top: 15),
                        child: CookbookDetailList(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 100,
              decoration: BoxDecoration(
                color: Color(0XFFDC0000),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(40),
                    bottomRight: Radius.circular(40)),
              ),
              child: Padding(
                padding: EdgeInsets.only(top: 30),
                child: Center(
                  child: Text(
                    ScannerService.searchValue,
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
          ),
          CookeryBackButton(),
          Positioned(
            right: 4.5,
            top: 39.0,
            child: PopupMenuButton(
              icon: Icon(Icons.more_vert, color: Colors.white),
              onSelected: (_) {
                if (_ == 'Đổi tên') {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Nhập tên mới cho Bộ sưu tập"),
                        content: TextFormField(),
                        actions: <Widget>[
                          MaterialButton(
                            elevation: 5,
                            child: Text("Cập nhật"),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      );
                    }
                  );
                }
              },
              itemBuilder: (BuildContext context) {
                return ['Đổi tên', 'Xóa'].map((_) => PopupMenuItem(value: _, child: Text(_))).toList();
              },
            ),
          ),
        ],
      ),
    );
  }
}