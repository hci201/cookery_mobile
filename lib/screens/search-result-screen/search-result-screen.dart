import 'package:cookery/screens/back_arrow/back_arrow.dart';
import 'package:cookery/screens/home-screen/components/suggestedFood.dart';
import 'package:cookery/services/scanner_service.dart';
import 'package:flutter/material.dart';

class SearchResultScreen extends StatefulWidget {
  SearchResultScreen({Key key}) : super(key: key);

  @override
  _SearchResultScreenState createState() => _SearchResultScreenState();
}

class _SearchResultScreenState extends State<SearchResultScreen> {
  List<TextEditingController> materialController = [
    'Bơ',
    'Trứng',
    'Hành Tây',
    'Cà Chua',
    'Thịt Xông Khói',
    'Măng Tây',
    'Tiêu Đen',
  ].map((_) => TextEditingController(text: _)).toList();
  List<FocusNode> _focusList = List.generate(7, (_) => FocusNode());
  List<bool> _confirmList = List.generate(7, (_) => true);
  bool _isConfirmed = false;
  // Future<void> _loadSuggest;
  bool _isProcessing = false;

  @override
  void dispose() {
    ScannerService.searchValue = null;
    ScannerService.isAnalysing = false;
    ScannerService.hideSearchResultTitle = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScannerService.isAnalysing ? Scaffold(
      body: Stack(
        children: <Widget>[
          CustomScrollView(
            physics: BouncingScrollPhysics(),
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: const EdgeInsets.only(top: 110),
                      child: Center(
                        child: Image.asset(
                          'images/IMG_2814.jpg',
                          height: 250,
                          width: MediaQuery.of(context).size.width - 40,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Container(
                        height: 1,
                        color: Colors.red[200],
                      ),
                    ),
                  ],
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 8.0, bottom: 10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Nguyên liệu đã nhận diện được',
                                  style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0XFF2A2A2A)),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 15),
                                  child: Text(
                                    'Vui lòng xác nhận lại các nguyên liệu đã được nhận diện sau đây trước khi tiếp tục',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontSize: 13,
                                        fontWeight: FontWeight.w300,
                                        color: Color(0XFF2A2A2A)),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            child: ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              padding: EdgeInsets.zero,
                              itemCount: 8,
                              itemBuilder: (context, index) {
                                return --index != -1 ? Padding(
                                  padding: EdgeInsets.only(bottom: 15),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey[300],
                                          blurRadius: 3,
                                          offset: Offset(0.5, 0.8),
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(3),
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.only(left: 2),
                                          child: Container(
                                            height: 50,
                                            width: 50,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft: Radius.circular(3),
                                                    topLeft: Radius.circular(3)),
                                                image: DecorationImage(
                                                    fit: BoxFit.cover, image: AssetImage('images/mat0${index + 1}.jpg'))),
                                          ),
                                        ),
                                        Expanded(
                                          child: GestureDetector(
                                            onTap: () {
                                              FocusScope.of(context).requestFocus(_focusList[index]);
                                            },
                                            child: Container(
                                              width: MediaQuery.of(context).size.width - 250,
                                              height: 54,
                                              child: Column(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Row(
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      Text(
                                                        '   -  ',
                                                        style: TextStyle(
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 16,
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: TextField(
                                                          focusNode: _focusList[index],
                                                          enabled: !_confirmList[index],
                                                          controller: this.materialController[index],
                                                          cursorColor: Theme.of(context).primaryColor,
                                                          decoration: InputDecoration.collapsed(
                                                            hintText: 'Tên nguyên liệu',
                                                          ),
                                                          maxLines: 1,
                                                          style: TextStyle(
                                                            fontFamily: 'Montserrat',
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 6,
                                                      ),
                                                      Switch(
                                                        onChanged: (val) => setState(() => _confirmList[index] = val),
                                                        value: _confirmList[index],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ) : Padding(
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: () {
                                          if (!_isConfirmed) {
                                            setState(() {
                                              _confirmList = List.generate(7, (_) => true);
                                            });
                                          }
                                        },
                                        child: Text(
                                          _confirmList.reduce((r, e) => r && e) ? '' : 'Xác nhận tất cả',
                                          style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 15,
                                            color: Theme.of(context).primaryColor,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: EdgeInsets.only(left: 10, right: 10, bottom: 110),
                      child: Container(
                        height: 1,
                        color: Colors.red[200],
                      ),
                    ),
                  ],
                ),
              ),
              // SliverList(
              //   delegate: SliverChildListDelegate(
              //     [
              //       !_isConfirmed ? Padding(
              //         padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20, bottom: 40),
              //         child: RaisedButton(
              //           color: Theme.of(context).primaryColor,
              //           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              //           elevation: 0,
              //           onPressed: () {
              //             _isConfirmed = true;
              //             _loadSuggest = Future.delayed(Duration(seconds: 2)).then((_) {});
              //             setState(() {});
              //           },
              //           child: Padding(
              //             padding: const EdgeInsets.symmetric(vertical: 15),
              //             child: Text(
              //             'Xem gợi ý món',
              //               style: TextStyle(
              //                   fontFamily: 'Montserrat',
              //                   fontSize: 20,
              //                   fontWeight: FontWeight.w300,
              //                   color: Colors.white),
              //             ),
              //           ),
              //         ),
              //       ) : Container(key: Key('buttonConf')),
              //     ],
              //   ),
              // ),
              // SliverList(
              //   delegate: SliverChildListDelegate(
              //     [
              //       _isConfirmed ? Container(
              //         padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 8.0),
              //         child: FutureBuilder(
              //           future: _loadSuggest,
              //           builder: (BuildContext context, AsyncSnapshot snapshot) {
              //             if (snapshot.connectionState == ConnectionState.done) {
              //               return Text(
              //                 'Gợi ý món ăn',
              //                 style: TextStyle(
              //                     fontFamily: 'Montserrat',
              //                     fontSize: 20,
              //                     fontWeight: FontWeight.w600,
              //                     color: Color(0XFF2A2A2A)),
              //               );
              //             }
              //             return Padding(
              //               padding: const EdgeInsets.symmetric(vertical: 20),
              //               child: Center(
              //                 child: Container(
              //                   width: 25,
              //                   height: 25,
              //                   child: CircularProgressIndicator(
              //                     strokeWidth: 2,
              //                   ),
              //                 ),
              //               ),
              //             );
              //           },
              //         ),
              //       ) : Container(key: Key('titleSug')),
              //     ],
              //   ),
              // ),
              // SliverList(
              //   delegate: SliverChildListDelegate(
              //     [
              //       _isConfirmed ? Padding(
              //         padding: EdgeInsets.only(top: 15),
              //         child: FutureBuilder(
              //           future: _loadSuggest,
              //           builder: (BuildContext context, AsyncSnapshot snapshot) {
              //             if (snapshot.connectionState == ConnectionState.done) {
              //               return SuggestedFoodScreen();
              //             }
              //             return SizedBox(
              //               height: 10,
              //             );
              //           },
              //         ),
              //       ) : Container(key: Key('contentSug')),
              //     ],
              //   ),
              // ),
            ],
          ),
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 100,
              decoration: BoxDecoration(
                color: Color(0XFFDC0000),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(40),
                    bottomRight: Radius.circular(40)),
              ),
              child: Padding(
                padding: EdgeInsets.only(top: 30),
                child: Center(
                  child: Text(
                    'Xác nhận nguyên liệu',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 75,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[400],
                    blurRadius: 3,
                    offset: Offset(-0.5, -0.8),
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  elevation: 0,
                  onPressed: () {
                    _isConfirmed = true;
                    // _loadSuggest = Future.delayed(Duration(seconds: 2)).then((_) {});
                    _isProcessing = true;
                    setState(() {});
                    Future.delayed(Duration(milliseconds: 800)).then((_) {
                      Navigator.pushReplacementNamed(context, '/suggest-result');
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15),
                    child: Text(
                    'Xem gợi ý món',
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 20,
                          fontWeight: FontWeight.w300,
                          color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ),
          CookeryBackButton(),
          _isProcessing ?
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.white30,
              child: Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                ),
              ),
            ),
          ) : Container(),
        ],
      ),
    ) : Scaffold(
      body: Stack(
        children: <Widget>[
          CustomScrollView(
            physics: BouncingScrollPhysics(),
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: const EdgeInsets.only(top: 100),
                      child: !ScannerService.hideSearchResultTitle ? Container(
                        padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 15),
                        child: Text(
                          'Kết quả tìm kiếm',
                          style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                              color: Color(0XFF2A2A2A)),
                        ),
                      ) : Container(),
                    ),
                  ],
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: EdgeInsets.only(top: 15),
                      child: SuggestedFoodScreen(),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 100,
              decoration: BoxDecoration(
                color: Color(0XFFDC0000),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(40),
                    bottomRight: Radius.circular(40)),
              ),
              child: Padding(
                padding: EdgeInsets.only(top: 30),
                child: Center(
                  child: Text(
                    ScannerService.searchValue ?? 'Màn hình kết quả',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
          ),
          CookeryBackButton(),
        ],
      ),
    );
  }
}