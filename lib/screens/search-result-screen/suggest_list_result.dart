import 'dart:math';

import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class SuggestListResult extends StatefulWidget {
  SuggestListResult({Key key}) : super(key: key);

  @override
  _SuggestListResultState createState() => _SuggestListResultState();
}

class _SuggestListResultState extends State<SuggestListResult> {
  List items = getDummyList();
  List images = getImageList();
  List foodNames = getFoodNameList();
  List materialNames = getMaterialNameList();
  List detailNames = getDetailList();
  List matchCount = [
    '2/2',
    '3/4',
    '3/4',
    '3/5',
    '4/6',
    '5/7',
    '4/8',
    '3/8',
    '4/10',
    '2/10'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: items.length,
      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 7.0, bottom: 7.0),
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () => Navigator.pushNamed(context, '/recipe2'),
          child: Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[300],
                    blurRadius: 3,
                    offset: Offset(0.5, 0.8),
                  ),
                ],
                borderRadius: BorderRadius.circular(3),
              ),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 2),
                    child: Container(
                      height: 116.0,
                      width: 116.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(3),
                              topLeft: Radius.circular(3)),
                          image: DecorationImage(
                              fit: BoxFit.cover, image: AssetImage(images[index]))),
                    ),
                  ),
                  Container(
                    height: 120,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            foodNames[index],
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                            ),
                          ),
                          Stack(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                                child: Container(
                                  width: MediaQuery.of(context).size.width - 180,
                                  child: Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.access_time,
                                        size: 15,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: 5),
                                        child: Text(
                                          materialNames[index],
                                          style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.w300,
                                            fontSize: 12,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                top: 5,
                                right: 0,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                    bottom: 4,
                                  ),
                                  child: SmoothStarRating(
                                    allowHalfRating: false,
                                    starCount: 5,
                                    rating: 5 - 2 * Random().nextDouble(),
                                    size: 10.0,
                                    color: Color(0XFFFFA127),
                                    borderColor: Color(0XFFFFA127),
                                    spacing: 0.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 5, 0, 2),
                            child: Container(
                              width: MediaQuery.of(context).size.width - 180,
                              child: Text(
                                detailNames[index],
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.justify,
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14,
                                  color: Colors.grey[500]
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 2),
                            child: Container(
                              width: MediaQuery.of(context).size.width - 180,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    matchCount[index],
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14,
                                      color: Colors.green
                                    ),
                                  ),
                                  Text(
                                    ' nguyên liệu có sẵn',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400,
                                      fontSize: 12,
                                      color: Colors.green
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    ));
  }

  static List getDummyList() {
    List list = List.generate(10, (i) {
      return "Item ${i + 1}";
    });
    return list;
  }

  static List getImageList() {
    List<String> list = List.generate(10, (i) {
      return "images/suggested/i${i + 1}.jpg";
    });
    return list;
  }

  static List getFoodNameList() {
    List<String> list = [
      'Trứng Ốp La',
      'Trứng Chiên Cà Chua',
      'Măng Tây Xào',
      'Canh Trứng',
      'Salad Bơ',
      'Cơm Chiên Dương Châu',
      'Gà Xáo Măng',
      'Trứng Chiên Thịt',
      'Rau Xào Tá Lả',
      'Bánh Hành'
    ];
    return list;
  }

  static List getMaterialNameList() {
    List<String> list = [
      '20p',
      '15p',
      '30p',
      '20p',
      '20p',
      '10p',
      '25p',
      '30p',
      '22p',
      '20p',
    ];
    return list;
  }

  static List getDetailList() {
    List<String> list = [
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt',
      'Gà nướng ăn cùng với rau răm, rau muối chua và cơm cháy đều rất tuyệt'
    ];
    return list;
  }
}