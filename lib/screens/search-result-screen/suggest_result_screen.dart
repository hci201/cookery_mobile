import 'package:cookery/screens/back_arrow/back_arrow.dart';
import 'package:cookery/screens/search-result-screen/suggest_list_result.dart';
import 'package:flutter/material.dart';

class SuggestResultScreen extends StatefulWidget {
  SuggestResultScreen({Key key}) : super(key: key);

  @override
  _SuggestResultScreenState createState() => _SuggestResultScreenState();
}

class _SuggestResultScreenState extends State<SuggestResultScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          CustomScrollView(
            physics: BouncingScrollPhysics(),
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: EdgeInsets.only(top: 115),
                      child: SuggestListResult(),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 100,
              decoration: BoxDecoration(
                color: Color(0XFFDC0000),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(40),
                    bottomRight: Radius.circular(40)),
              ),
              child: Padding(
                padding: EdgeInsets.only(top: 30),
                child: Center(
                  child: Text(
                    'Gợi ý món ăn',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
          ),
          CookeryBackButton(),
        ],
      ),
    );
  }
}