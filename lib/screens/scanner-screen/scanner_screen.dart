import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/services.dart';
import 'package:icon_shadow/icon_shadow.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:cookery/services/scanner_service.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:photo_manager/photo_manager.dart';

class ScannerScreen extends StatefulWidget {
  ScannerScreen({Key key}) : super(key: key);

  @override
  _ScannerScreenState createState() => _ScannerScreenState();
}

class _ScannerScreenState extends State<ScannerScreen> {
  List<CameraDescription> cameras;
  CameraController controller;
  Future<void> initializeControllerFuture;
  bool isCameraFront;
  double currentScale, lastScale;
  Future<File> imageAssetFile;

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);

    this.currentScale = 1.0;
    this.lastScale = 1.0;

    PermissionHandler().requestPermissions([PermissionGroup.camera, PermissionGroup.microphone, PermissionGroup.storage]).then((Map<PermissionGroup, PermissionStatus> permissions) {
      bool isGranted = permissions[PermissionGroup.camera] == PermissionStatus.granted;
      if (!isGranted) {
        showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context) => AlertDialog(
            title: Text('Lỗi máy ảnh'),
            content: Text('Vui lòng cấp quyền truy cập máy ảnh để tiến hành quét và phân tích.'),
            actions: <Widget>[
              RaisedButton(
                color: Colors.white,
                onPressed: () => Navigator.pop(context),
                child: Text('OK', style: TextStyle(color: Colors.red)),
              ),
            ],
          ),
        );
      } else {
        ScannerService.getAvailableCameras().then((availableCameras) {
          this.cameras = availableCameras;
          if (this.cameras.length > 0) {
            this.setCamera();
            this.imageAssetFile = PhotoManager.requestPermission().then((result) {
              if (result) {
                return PhotoManager.getImageAsset().then((assetPaths) {
                  if (assetPaths == null || assetPaths.length == 0) return null;
                  return assetPaths[0].assetList.then((assetList) {
                    return assetList[0].file;
                  });
                });
              } else {
                return null;
              }
            });
          }
        });
      }
    });
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    if (this.controller != null) {
      this.controller.dispose();
    }
    super.dispose();
  }
  
  Future<void> setCamera({String cameraType = 'rear'}) async {
    var currentCamera;
    if (cameraType == 'rear') {
      currentCamera = this.cameras[0];
      this.isCameraFront = false;
    } else if (cameraType == 'front') {
      if (this.cameras.length > 1) {
        currentCamera = this.cameras[1];
        this.isCameraFront = true;
      }
    }
    if (currentCamera != null) {
      if (this.controller != null) {
        await this.controller.dispose();
      }
      // To display the current output from the Camera,
      // create a CameraController.
      this.controller = CameraController(
        // Get a specific camera from the list of available cameras.
        currentCamera,
        // Define the resolution to use.
        ResolutionPreset.max,
      );

      this.controller.addListener(() {
        if (mounted) {
          setState(() {});
        }

        if (this.controller.value.hasError) {
          print('Camera error ${this.controller.value.errorDescription}');
        }
      });

      try {
        // Next, initialize the controller. This returns a Future.
        this.initializeControllerFuture = this.controller.initialize();
      } on CameraException catch (e) {
        print('Caught CameraException: ${e.description}');
      }

      if (mounted) {
        setState(() {});
      }
    }
  }

  Future<void> switchCamera() async {
    this.currentScale = 1.0;
    this.lastScale = 1.0;
    this.isCameraFront ? await this.setCamera(cameraType: 'rear') : await this.setCamera(cameraType: 'front');
  }

  Future<void> takePicture() async {
    // Take the Picture in a try / catch block. If anything goes wrong,
    // catch the error.
    try {
      // Ensure that the camera is initialized.
      await this.initializeControllerFuture;

      // Construct the path where the image should be saved using the
      // pattern package.
      final path = join(
        // Store the picture in the temp directory.
        // Find the temp directory using the `path_provider` plugin.
        (await getTemporaryDirectory()).path,
        '${DateTime.now()}.png',
      );

      // Attempt to take a picture and log where it's been saved.
      await this.controller.takePicture(path);

      ScannerService.aspectRatio = this.controller.value.aspectRatio;
      ScannerService.imagePath = path;
      ScannerService.sourceType = 'camera';
      this.currentScale = 1.0;
      this.lastScale = 1.0;

      // If the picture was taken, display it on a new screen.
      Navigator.pushNamed(context, '/review');
    } catch(e) {
      // If an error occurs, log the error to the console.
      print(e);
    }
  }

  Future<void> pickImage() async {
    try {
      final imageFile = await ImagePicker.pickImage(
        source: ImageSource.gallery,
      );
      ScannerService.aspectRatio = this.controller.value.aspectRatio;
      ScannerService.imagePath = imageFile.path;
      ScannerService.sourceType = 'gallery';

      Navigator.pushNamed(context, '/review');
    } catch(e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        FutureBuilder<void>(
          future: this.initializeControllerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              // If the Future is complete, display the preview.
              return GestureDetector(
                onScaleUpdate: (_) {
                  double newScale = this.lastScale * _.scale;
                  if (newScale < 1.0) {
                    newScale = 1.0;
                  } else if (newScale > 3.0) {
                    newScale = 3.0;
                  }
                  setState(() {
                    this.currentScale = newScale;
                  });
                },
                onScaleEnd: (_) {
                  this.lastScale = this.currentScale;
                },
                child: Transform.scale(
                  scale: this.currentScale,
                  child: AspectRatio(
                    aspectRatio: this.controller.value.aspectRatio,
                    child: CameraPreview(this.controller),
                  ),
                ),
              );
            }
            // Otherwise, display a loading indicator.
            return Center(child: CircularProgressIndicator());
          },
        ),
        Positioned(
          child: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Container(
              width: 67,
              height: 67,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: IconShadowWidget(
                          Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 32,
                          ),
                          shadowColor: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          top: 10,
          right: 0,
        ),
        Positioned(
          child: GestureDetector(
            onTap: this.switchCamera,
            child: Container(
              width: 67,
              height: 67,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: IconShadowWidget(
                          Icon(
                            Icons.autorenew,
                            color: Colors.white,
                            size: 32,
                          ),
                          shadowColor: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          bottom: 0,
          right: 0,
        ),
        Positioned(
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Positioned(
                child: Container(
                  width: 68,
                  height: 68,
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    shape: BoxShape.circle,
                    border: Border.all(width: 3, color: Colors.white),
                  ),
                ),
              ),
              FloatingActionButton(
                splashColor: Colors.red[200],
                child: Icon(Icons.filter_center_focus, color: Colors.red, size: 20),
                // Provide an onPressed callback.
                onPressed: this.takePicture,
                elevation: 0,
                backgroundColor: Colors.white,
              ),
            ],
          ),
          bottom: 25,
          right: MediaQuery.of(context).size.width / 2 - 34,
        ),
        Positioned(
          child: GestureDetector(
            onTap: this.pickImage,
            child: Container(
              width: 76,
              height: 76,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: FutureBuilder<File>(
                          future: this.imageAssetFile,
                          builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
                            if (snapshot.hasData) {
                              return Container(
                                width: 42,
                                height: 42,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 3,
                                    color: Colors.white,
                                  ),
                                  borderRadius: BorderRadius.circular(8),
                                  image: DecorationImage(
                                    image: FileImage(snapshot.data),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              );
                            }
                            return Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 3,
                                      color: Colors.white,
                                    ),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                                Icon(
                                  Icons.image,
                                  size: 42,
                                  color: Colors.white,
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          bottom: 0,
          left: 0,
        ),
      ],
    );
  }
}
