import 'dart:async';
import 'dart:io';

import 'package:cookery/services/scanner_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:icon_shadow/icon_shadow.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:toast/toast.dart';

class ReviewScreen extends StatefulWidget {
  ReviewScreen({Key key}) : super(key: key);

  @override
  _ReviewScreenState createState() => _ReviewScreenState();
}

class _ReviewScreenState extends State<ReviewScreen> {
  double aspectRatio;
  String imagePath;
  File imageFile;
  bool isSaving;
  bool showSaveImageButton;
  Timer timer;
  double _percentage = 0.0;
  double _nextPercentage = 0.0;

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);

    this.isSaving = false;
    this.aspectRatio = ScannerService.aspectRatio;
    this.imagePath = ScannerService.imagePath;
    this.imageFile = File(this.imagePath);
    this.showSaveImageButton = ScannerService.sourceType == 'camera';
    this.timer = null;
    Timer.periodic(Duration(milliseconds: 50), this.handleTicker);
  }

  @override
  void dispose() { 
    if (this.timer != null && this.timer.isActive) {
      this.timer.cancel();
    }
    super.dispose();
  }

  void handleTicker(Timer _timer) {
    this.timer = _timer;
    if (_nextPercentage <= 1.0) {
      this.publishProgress();
    }
  }

  void publishProgress() {
    if (_percentage == 1.0) {
      this.timer.cancel();
      Future.delayed(Duration(milliseconds: 500)).then((_) {
        ScannerService.searchValue = 'Xác nhận nguyên liệu';
        ScannerService.isAnalysing = true;
        SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
        // Future.sync(() => Navigator.pop(context))
        //   // .then((_) => Navigator.pop(context))
        //   .then((_) => );
        Navigator.pushReplacementNamed(context, '/search-result');
      });
    } else {
      setState(() {
        _percentage = _nextPercentage;
        _nextPercentage += 0.01;
        if (_nextPercentage > 1.0) {
          _nextPercentage = 1.0;
        }
      });
    }
  }

  Future<void> saveImage() async {
    setState(() {
      this.isSaving = true;
    });
    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);
    bool isGranted = permission == PermissionStatus.granted;
    if (!isGranted) {
      Map<PermissionGroup, PermissionStatus> permissions = await PermissionHandler().requestPermissions([PermissionGroup.storage]);
      isGranted = permissions[PermissionGroup.storage] == PermissionStatus.granted;
      if (!isGranted) {
        showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context) => AlertDialog(
            title: Text('Lỗi lưu ảnh'),
            content: Text('Vui lòng cấp quyền truy cập để lưu ảnh này trên thiết bị của bạn.'),
            actions: <Widget>[
              RaisedButton(
                color: Colors.white,
                onPressed: () => Navigator.pop(context),
                child: Text('OK', style: TextStyle(color: Colors.red)),
              ),
            ],
          ),
        ).whenComplete(() {
          setState(() {
            this.isSaving = false;
          });
        });
      }
    }
    if (isGranted) {
      final resultSuccess = await GallerySaver.saveImage(this.imagePath);
      setState(() {
        this.isSaving = false;
      });
      if (resultSuccess) {
        Toast.show(
          'Đã lưu',
          context,
          duration: Toast.LENGTH_LONG,
          gravity:  Toast.CENTER,
          backgroundColor: Colors.red.withOpacity(0.8),
          textColor: Colors.white,
        );
      } else {
        Toast.show(
          'Đã có lỗi xảy ra!',
          context,
          duration: Toast.LENGTH_LONG,
          gravity:  Toast.CENTER,
          backgroundColor: Colors.red.withOpacity(0.8),
          textColor: Colors.white,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AspectRatio(
          aspectRatio: this.aspectRatio,
          child: Image.file(this.imageFile),
        ),
        Positioned(
          child: GestureDetector(
            onTap: () {
              this.imageFile.delete(recursive: true);
              Navigator.pop(context);
            },
            child: Container(
              width: 67,
              height: 67,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5, right: 8),
                        child: IconShadowWidget(
                          Icon(
                            Icons.keyboard_arrow_left,
                            color: Colors.white,
                            size: 40,
                          ),
                          shadowColor: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          top: 10,
          left: 0,
        ),
        this.showSaveImageButton ?
        Positioned(
          child: GestureDetector(
            onTap: this.saveImage,
            child: Container(
              width: 67,
              height: 67,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: IconShadowWidget(
                          Icon(
                            Icons.vertical_align_bottom,
                            color: Colors.white,
                            size: 32,
                          ),
                          shadowColor: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          bottom: 0,
          left: 0,
        ) :
        Container(),
        Positioned(
          top: 0,
          left: 0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.white30,
            child: Center(
              child: CircularPercentIndicator(
                radius: 150,
                lineWidth: 6,
                percent: _percentage,
                center: Text(
                  '${(_percentage * 100).toInt()}%',
                  style: TextStyle(
                    color: _percentage == 1.0 ? Theme.of(context).primaryColor : Colors.white,
                    fontFamily: 'Montserrat',
                    fontSize: 30,
                    fontWeight: FontWeight.w900,
                  ),
                ),
                progressColor: Theme.of(context).primaryColor,
                backgroundColor: Colors.white54,
              ),
            ),
          ),
        ),
        // Positioned(
        //   child: GestureDetector(
        //     onTap: () {
        //       ScannerService.searchValue = 'Xác nhận nguyên liệu';
        //       ScannerService.isAnalysing = true;
        //       SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
        //       Future.sync(() => Navigator.pop(context))
        //         // .then((_) => Navigator.pop(context))
        //         .then((_) => Navigator.pushNamed(context, '/search-result'));
        //     },
        //     child: Container(
        //       decoration: BoxDecoration(
        //         color: Colors.white,
        //         borderRadius: BorderRadius.circular(10),
        //         boxShadow: [
        //           BoxShadow(
        //             color: Colors.grey,
        //             blurRadius: 3,
        //           ),
        //         ],
        //       ),
        //       child: Row(
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         children: <Widget>[
        //           Padding(
        //             padding: const EdgeInsets.only(left: 15),
        //             child: Text(
        //               'Phân tích',
        //               style: TextStyle(
        //                 color: Colors.black,
        //                 fontSize: 16,
        //                 decoration: TextDecoration.none,
        //               ),
        //             ),
        //           ),
        //           Padding(
        //             padding: const EdgeInsets.all(10),
        //             child: Icon(
        //               Icons.send,
        //               color: Colors.black,
        //               size: 30,
        //             ),
        //           ),
        //         ],
        //       ),
        //     ),
        //   ),
        //   bottom: 15,
        //   right: 15,
        // ),
      ],
    );
  }
}