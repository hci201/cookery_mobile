import 'package:flutter/material.dart';

class CookeryLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text("Bạn vui lòng đăng nhập để sử dụng chức năng"),
//        backgroundColor: Colors.red,
//      ),
//      body: Container(

  BoxDecoration myBoxDecoration(){
    return BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
    );
  }

   return Container(
       color: Color.fromRGBO(192, 192, 192, 0.5),
       child: Padding(
         padding: EdgeInsets.only(top: 200, bottom: 200, left: 17, right: 17),
         child: Container(
             decoration: BoxDecoration(
                 border: Border.all(),
                 borderRadius: BorderRadius.circular(10),
                 color: Colors.white
             ),
             child: Column(
                 mainAxisAlignment: MainAxisAlignment.center,
                 crossAxisAlignment: CrossAxisAlignment.center,
                 children: <Widget>[
                   Padding(
                     padding: EdgeInsets.only(bottom: 10, left: 15, right: 15),
                     child: Text('Bạn vui lòng đăng nhập để có thể sử dụng thêm nhiều tính năng của ứng dụng',textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold),),
                   ),
                   Container(
                       child: GestureDetector(
                         onTap: (){},
                         child: Container(
                           child: Column(
                             children: <Widget>[
                               Padding(
                                 padding: EdgeInsets.only(bottom: 8, left: 10, right: 10),
                                 child: Container(
                                   height: 36,
                                   color: Colors.white,
                                   //decoration: myBoxDecoration(),
                                   child: Row(
                                     children: <Widget>[
                                       Container(
                                         height: 36,
                                         width: 36,
                                         decoration: BoxDecoration(
                                             image: DecorationImage(
                                                 image: AssetImage('images/icons/g.jpg'),
                                                 fit: BoxFit.cover
                                             )
                                         ),
                                       ),
                                       Padding(
                                         padding: EdgeInsets.only(left: 15, top: 4, bottom: 4),
                                         child:Text('Đăng nhập bằng google', textAlign: TextAlign.center, style: TextStyle(fontSize: 18),),
                                       )
                                     ],
                                   ),
                                 ),
                               ),
                               Padding(
                                 padding: EdgeInsets.only(bottom: 8, left: 10, right: 10),
                                 child: Container(
                                   height: 36,
                                   color: Colors.blue,
                                   // decoration: myBoxDecoration(),
                                   child: Row(
                                     children: <Widget>[
                                       Container(
                                         height: 36,
                                         width: 36,
                                         decoration: BoxDecoration(
                                             image: DecorationImage(
                                                 image: AssetImage('images/icons/f.jpg'),
                                                 fit: BoxFit.cover
                                             )
                                         ),
                                       ),
                                       Padding(
                                         padding: EdgeInsets.only(left: 15, top: 4, bottom: 4),
                                         child:Text('Đăng nhập bằng facebook', textAlign: TextAlign.center, style: TextStyle(fontSize: 18),),
                                       )
                                     ],
                                   ),
                                 ),
                               ),
                               Padding(
                                 padding: EdgeInsets.only(bottom: 8, left: 10, right: 10),
                                 child: Container(
                                   height: 36,
                                   color: Colors.red,
                                   //decoration: myBoxDecoration(),
                                   child: Row(
                                     children: <Widget>[
                                       Container(
                                         height: 36,
                                         width: 36,
                                         decoration: BoxDecoration(
                                             image: DecorationImage(
                                                 image: AssetImage('images/icons/i.jpg'),
                                                 fit: BoxFit.cover
                                             )
                                         ),
                                       ),
                                       Padding(
                                         padding: EdgeInsets.only(left: 15, top: 4),
                                         child:Text('Đăng nhập bằng instagram', textAlign: TextAlign.center, style: TextStyle(fontSize: 18),),
                                       )
                                     ],
                                   ),
                                 ),
                               )
                             ],
                           ),
                         ),
                       )
                   ),
                 ]
             )
         ),
       )
   );
  }
}
