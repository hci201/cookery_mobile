import 'package:flutter/material.dart';

class CookeryBackButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 4.5,
      top: 39.0,
      child: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.white,),
        onPressed: () {
          Navigator.pop(
              context);
        },
      ),
    );
  }
}
